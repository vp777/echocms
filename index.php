
<?php

include('session.php');

header('location:app-contacts.php');

?>


<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="assets/css/apexcharts.css" />
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
include("ui/preloader.php");
?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
          <div class="navbar-header">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a
              class="nav-toggler waves-effect waves-light d-block d-md-none"
              href="javascript:void(0)"
            >
              <i class="ri-close-line fs-6 ri-menu-2-line"></i>
            </a>
            <!-- -------------------------------------------------------------- -->
            <!-- Logo -->
            <!-- -------------------------------------------------------------- -->
            <a class="navbar-brand" href="index.php">
              <!-- Logo icon -->
              <b class="logo-icon">
                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Dark Logo icon -->
                <img src="assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                <!-- Light Logo icon -->
                <img
                  src="assets/images/logo-light-icon.png"
                  alt="homepage"
                  class="light-logo"
                />
              </b>
              <!--End Logo icon -->
              <!-- Logo text -->
              <span class="logo-text">
                <!-- dark Logo text -->
                <img src="assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                <!-- Light Logo text -->
                <img
                  src="assets/images/logo-light-text.png"
                  class="light-logo"
                  alt="homepage"
                />
              </span>
            </a>
            <!-- -------------------------------------------------------------- -->
            <!-- End Logo -->
            <!-- -------------------------------------------------------------- -->
            <!-- -------------------------------------------------------------- -->
            <!-- Toggle which is visible on mobile only -->
            <!-- -------------------------------------------------------------- -->
            <a
              class="topbartoggler d-block d-md-none waves-effect waves-light"
              href="javascript:void(0)"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
              ><i class="ri-more-line fs-6"></i
            ></a>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- End Logo -->
          <!-- -------------------------------------------------------------- -->
          <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <!-- -------------------------------------------------------------- -->
            <!-- toggle and nav items -->
            <!-- -------------------------------------------------------------- -->
            <ul class="navbar-nav me-auto">
              <li class="nav-item d-none d-md-block">
                <a
                  class="nav-link sidebartoggler waves-effect waves-light"
                  href="javascript:void(0)"
                  data-sidebartype="mini-sidebar"
                  ><i data-feather="menu" class="feather-sm"></i
                ></a>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- mega menu -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown mega-dropdown">
                <a
                  class="nav-link dropdown-toggle waves-effect waves-dark"
                  role="button"
                  href="#"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span class="d-none d-md-block"
                    >Mega <i data-feather="chevron-down" class="feather-sm"></i
                  ></span>
                  <span class="d-block d-md-none"><i class="ri-keyboard-line"></i></span>
                </a>
                <div class="dropdown-menu dropdown-menu-animate-up">
                  <div class="mega-dropdown-menu row">
                    <div class="col-lg-3 col-xl-2 mb-4">
                      <h4 class="mb-3">Carousel</h4>
                      <!-- CAROUSEL -->
                      <div
                        id="carouselExampleControls"
                        class="carousel carousel-dark slide"
                        data-bs-ride="carousel"
                      >
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img
                              class="d-block img-fluid"
                              src="assets/images/img1.jpg"
                              alt="First slide"
                            />
                          </div>
                          <div class="carousel-item">
                            <img
                              class="d-block img-fluid"
                              src="assets/images/img2.jpg"
                              alt="Second slide"
                            />
                          </div>
                          <div class="carousel-item">
                            <img
                              class="d-block img-fluid"
                              src="assets/images/img3.jpg"
                              alt="Third slide"
                            />
                          </div>
                        </div>
                        <a
                          class="carousel-control-prev"
                          href="#carouselExampleControls"
                          role="button"
                          data-bs-slide="prev"
                        >
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </a>
                        <a
                          class="carousel-control-next"
                          href="#carouselExampleControls"
                          role="button"
                          data-bs-slide="next"
                        >
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </a>
                      </div>
                      <!-- End CAROUSEL -->
                    </div>
                    <div class="col-lg-3 mb-4">
                      <h4 class="mb-3">Accordian</h4>
                      <!-- Accordian -->
                      <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="flush-headingOne">
                            <button
                              class="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#flush-collapseOne"
                              aria-expanded="false"
                              aria-controls="flush-collapseOne"
                            >
                              Accordion Item #1
                            </button>
                          </h2>
                          <div
                            id="flush-collapseOne"
                            class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne"
                            data-bs-parent="#accordionFlushExample"
                          >
                            <div class="accordion-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                              terry richardson ad squid.
                            </div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="flush-headingTwo">
                            <button
                              class="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#flush-collapseTwo"
                              aria-expanded="false"
                              aria-controls="flush-collapseTwo"
                            >
                              Accordion Item #2
                            </button>
                          </h2>
                          <div
                            id="flush-collapseTwo"
                            class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo"
                            data-bs-parent="#accordionFlushExample"
                          >
                            <div class="accordion-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                              terry richardson ad squid.
                            </div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="flush-headingThree">
                            <button
                              class="accordion-button collapsed"
                              type="button"
                              data-bs-toggle="collapse"
                              data-bs-target="#flush-collapseThree"
                              aria-expanded="false"
                              aria-controls="flush-collapseThree"
                            >
                              Accordion Item #3
                            </button>
                          </h2>
                          <div
                            id="flush-collapseThree"
                            class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree"
                            data-bs-parent="#accordionFlushExample"
                          >
                            <div class="accordion-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                              terry richardson ad squid.
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                      <h4 class="mb-3">Contact Us</h4>
                      <!-- Contact -->
                      <form>
                        <div class="mb-3 form-floating">
                          <input
                            type="text"
                            class="form-control"
                            id="exampleInputname1"
                            placeholder="Enter Name"
                          />
                          <label>Enter Name</label>
                        </div>
                        <div class="mb-3 form-floating">
                          <input type="email" class="form-control" placeholder="Enter email" />
                          <label>Enter Email address</label>
                        </div>
                        <div class="mb-3 form-floating">
                          <textarea
                            class="form-control"
                            id="exampleTextarea"
                            rows="3"
                            placeholder="Message"
                          ></textarea>
                          <label>Enter Message</label>
                        </div>
                        <button type="submit" class="btn px-4 rounded-pill btn-info">Submit</button>
                      </form>
                    </div>
                    <div class="col-lg-3 col-xlg-4 mb-4">
                      <h4 class="mb-3">List style</h4>
                      <!-- List style -->
                      <ul class="list-style-none">
                        <li>
                          <a href="#"
                            ><i
                              data-feather="check-circle"
                              class="feather-sm text-success me-2"
                            ></i>
                            You can give link</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            ><i
                              data-feather="check-circle"
                              class="feather-sm text-success me-2"
                            ></i>
                            Give link</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            ><i
                              data-feather="check-circle"
                              class="feather-sm text-success me-2"
                            ></i>
                            Another Give link</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            ><i
                              data-feather="check-circle"
                              class="feather-sm text-success me-2"
                            ></i>
                            Forth link</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            ><i
                              data-feather="check-circle"
                              class="feather-sm text-success me-2"
                            ></i>
                            Another fifth link</a
                          >
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- End mega menu -->
              <!-- -------------------------------------------------------------- -->
              <!-- -------------------------------------------------------------- -->
              <!-- create new -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span class="d-none d-md-block"
                    >Create New <i data-feather="chevron-down" class="feather-sm"></i
                  ></span>
                  <span class="d-block d-md-none"
                    ><i data-feather="plus" class="feather-sm"></i
                  ></span>
                </a>
                <div
                  class="dropdown-menu dropdown-menu-animate-up"
                  aria-labelledby="navbarDropdown"
                >
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- Search -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item search-box">
                <a class="nav-link waves-effect waves-dark" href="javascript:(0)"
                  ><i data-feather="search" class="feather-sm"></i
                ></a>
                <form class="app-search position-absolute">
                  <input type="text" class="form-control" placeholder="Search &amp; enter" />
                  <a class="srh-btn"><i data-feather="x" class="feather-sm"></i></a>
                </form>
              </li>
            </ul>
            <!-- -------------------------------------------------------------- -->
            <!-- Right side toggle and nav items -->
            <!-- -------------------------------------------------------------- -->
            <ul class="navbar-nav">
              <!-- -------------------------------------------------------------- -->
              <!-- create new -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown2"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="flag-icon flag-icon-us"></i>
                </a>
                <div
                  class="dropdown-menu dropdown-menu-right dropdown-menu-animate-up"
                  aria-labelledby="navbarDropdown2"
                >
                  <a class="dropdown-item" href="#"
                    ><i class="me-2 flag-icon flag-icon-us"></i> English</a
                  >
                  <a class="dropdown-item" href="#"
                    ><i class="me-2 flag-icon flag-icon-fr"></i> French</a
                  >
                  <a class="dropdown-item" href="#"
                    ><i class="me-2 flag-icon flag-icon-es"></i> Spanish</a
                  >
                  <a class="dropdown-item" href="#"
                    ><i class="me-2 flag-icon flag-icon-de"></i> German</a
                  >
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- Comment -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle waves-effect waves-dark"
                  href=""
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i data-feather="bell" class="feather-sm"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-end mailbox dropdown-menu-animate-up">
                  <span class="with-arrow"><span class="bg-primary"></span></span>
                  <ul class="list-style-none">
                    <li>
                      <div class="drop-title bg-primary text-white">
                        <h4 class="mb-0 mt-1">4 New</h4>
                        <span class="fw-light">Notifications</span>
                      </div>
                    </li>
                    <li>
                      <div class="message-center notifications">
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="btn btn-light-danger text-danger btn-circle">
                            <i data-feather="link" class="feather-sm fill-white"></i>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Luanch Admin</h5>
                            <span class="mail-desc">Just see the my new admin!</span>
                            <span class="time">9:30 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="btn btn-light-success text-success btn-circle">
                            <i data-feather="calendar" class="feather-sm fill-white"></i>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Event today</h5>
                            <span class="mail-desc">Just a reminder that you have event</span>
                            <span class="time">9:10 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="btn btn-light-info text-info btn-circle">
                            <i data-feather="settings" class="feather-sm fill-white"></i>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Settings</h5>
                            <span class="mail-desc"
                              >You can customize this template as you want</span
                            >
                            <span class="time">9:08 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="btn btn-light-primary text-primary btn-circle">
                            <i data-feather="users" class="feather-sm fill-white"></i>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Pavan kumar</h5>
                            <span class="mail-desc">Just see the my admin!</span>
                            <span class="time">9:02 AM</span>
                          </div>
                        </a>
                      </div>
                    </li>
                    <li>
                      <a class="nav-link text-center mb-1 text-dark" href="#">
                        <strong>Check all notifications</strong>
                        <i data-feather="chevron-right" class="feather-sm"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- End Comment -->
              <!-- -------------------------------------------------------------- -->
              <!-- -------------------------------------------------------------- -->
              <!-- Messages -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle waves-effect waves-dark"
                  href=""
                  id="2"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i data-feather="message-square" class="feather-sm"></i>
                </a>
                <div
                  class="dropdown-menu dropdown-menu-end mailbox dropdown-menu-animate-up"
                  aria-labelledby="2"
                >
                  <span class="with-arrow"><span class="bg-danger"></span></span>
                  <ul class="list-style-none">
                    <li>
                      <div class="drop-title text-white bg-danger">
                        <h4 class="mb-0 mt-1">5 New</h4>
                        <span class="fw-light">Messages</span>
                      </div>
                    </li>
                    <li>
                      <div class="message-center message-body">
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="user-img">
                            <img
                              src="assets/images/1.jpg"
                              alt="user"
                              class="rounded-circle"
                            />
                            <span class="profile-status online pull-right"></span>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Pavan kumar</h5>
                            <span class="mail-desc">Just see the my admin!</span>
                            <span class="time">9:30 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="user-img">
                            <img
                              src="assets/images/2.jpg"
                              alt="user"
                              class="rounded-circle"
                            />
                            <span class="profile-status busy pull-right"></span>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Sonu Nigam</h5>
                            <span class="mail-desc">I've sung a song! See you at</span>
                            <span class="time">9:10 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="user-img">
                            <img
                              src="assets/images/3.jpg"
                              alt="user"
                              class="rounded-circle"
                            />
                            <span class="profile-status away pull-right"></span>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Arijit Sinh</h5>
                            <span class="mail-desc">I am a singer!</span>
                            <span class="time">9:08 AM</span>
                          </div>
                        </a>
                        <!-- Message -->
                        <a href="#" class="message-item">
                          <span class="user-img">
                            <img
                              src="assets/images/4.jpg"
                              alt="user"
                              class="rounded-circle"
                            />
                            <span class="profile-status offline pull-right"></span>
                          </span>
                          <div class="mail-contnet">
                            <h5 class="message-title">Pavan kumar</h5>
                            <span class="mail-desc">Just see the my admin!</span>
                            <span class="time">9:02 AM</span>
                          </div>
                        </a>
                      </div>
                    </li>
                    <li>
                      <a class="nav-link text-center link text-dark" href="#">
                        <b>See all e-Mails</b>
                        <i data-feather="chevron-right" class="feather-sm"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- End Messages -->
              <!-- -------------------------------------------------------------- -->
              <!-- -------------------------------------------------------------- -->
              <!-- User profile and search -->
              <!-- -------------------------------------------------------------- -->
              <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic"
                  href=""
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  ><img
                    src="assets/images/1.jpg"
                    alt="user"
                    class="rounded-circle"
                    width="31"
                /></a>
                <div class="dropdown-menu dropdown-menu-end user-dd animated flipInY">
                  <span class="with-arrow"><span class="bg-primary"></span></span>
                  <div class="d-flex no-block align-items-center p-3 bg-primary text-white mb-2">
                    <div class="">
                      <img
                        src="assets/images/1.jpg"
                        alt="user"
                        class="rounded-circle"
                        width="60"
                      />
                    </div>
                    <div class="ms-2">
                      <h4 class="mb-0">Steave Jobs</h4>
                      <p class="mb-0">varun@gmail.com</p>
                    </div>
                  </div>
                  <a class="dropdown-item" href="#"
                    ><i data-feather="user" class="feather-sm text-info me-1 ms-1"></i> My
                    Profile</a
                  >
                  <a class="dropdown-item" href="#"
                    ><i data-feather="credit-card" class="feather-sm text-primary me-1 ms-1"></i> My
                    Balance</a
                  >
                  <a class="dropdown-item" href="#"
                    ><i data-feather="mail" class="feather-sm text-success me-1 ms-1"></i> Inbox</a
                  >
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#"
                    ><i data-feather="settings" class="feather-sm text-warning me-1 ms-1"></i>
                    Account Setting</a
                  >
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#"
                    ><i data-feather="log-out" class="feather-sm text-danger me-1 ms-1"></i>
                    Logout</a
                  >
                  <div class="dropdown-divider"></div>
                  <div class="pl-4 p-2">
                    <a href="#" class="btn d-block w-100 btn-primary rounded-pill">View Profile</a>
                  </div>
                </div>
              </li>
              <!-- -------------------------------------------------------------- -->
              <!-- User profile and search -->
              <!-- -------------------------------------------------------------- -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
          <!-- Sidebar navigation-->
          <nav class="sidebar-nav">
            <ul id="sidebarnav">
              <!-- User Profile-->
              <li>
                <!-- User Profile-->
                <div class="user-profile d-flex no-block dropdown mt-3">
                  <div class="user-pic">
                    <img
                      src="assets/images/1.jpg"
                      alt="users"
                      class="rounded-circle"
                      width="40"
                    />
                  </div>
                  <div class="user-content hide-menu ms-2">
                    <a
                      href="#"
                      class=""
                      id="Userdd"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <h5 class="mb-0 user-name font-medium">
                        Steave Jobs
                        <i data-feather="chevron-down" class="feather-sm"></i>
                      </h5>
                      <span class="op-5 user-email">varun@gmail.com</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Userdd">
                      <a class="dropdown-item" href="#"
                        ><i data-feather="user" class="feather-sm text-info me-1"></i> My Profile</a
                      >
                      <a class="dropdown-item" href="#"
                        ><i data-feather="credit-card" class="feather-sm text-primary me-1"></i> My
                        Balance</a
                      >
                      <a class="dropdown-item" href="#"
                        ><i data-feather="mail" class="feather-sm text-success me-1"></i> Inbox</a
                      >
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#"
                        ><i data-feather="settings" class="feather-sm text-warning me-1"></i>
                        Account Setting</a
                      >
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#"
                        ><i data-feather="log-out" class="feather-sm text-danger me-1"></i>
                        Logout</a
                      >
                    </div>
                  </div>
                </div>
                <!-- End User Profile-->
              </li>
              <li class="p-3 mt-2">
                <a
                  href="#"
                  class="btn btn-block create-btn text-white no-block d-flex align-items-center"
                  ><i data-feather="plus-square" class="feather-sm"></i>
                  <span class="hide-menu ms-1">Create New</span>
                </a>
              </li>
              <!-- User Profile-->
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Personal</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="home" class="feather-icon"></i
                  ><span class="hide-menu">Dashboard </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="index.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Classic </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index2.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Analytical </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index3.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i
                      ><span class="hide-menu"> Cryptocurrency </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index4.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Overview </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index5.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> E-Commerce </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index6.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Sales </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index7.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> General </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index8.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Trendy </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index9.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Campaign </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="index10.php" class="sidebar-link"
                      ><i class="mdi mdi-adjust"></i><span class="hide-menu"> Modern </span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="sidebar" class="feather-icon"></i
                  ><span class="hide-menu">Sidebar Type </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="sidebar-type-minisidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-quilt"></i
                      ><span class="hide-menu"> Minisidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="sidebar-type-iconsidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-parallel"></i
                      ><span class="hide-menu"> Icon Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="sidebar-type-overlaysidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-day"></i
                      ><span class="hide-menu"> Overlay Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="sidebar-type-fullsidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-array"></i
                      ><span class="hide-menu"> Full Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="sidebar-type-horizontalsidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-module"></i
                      ><span class="hide-menu"> Horizontal Sidebar </span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="clipboard" class="feather-icon"></i
                  ><span class="hide-menu">Page Layouts </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="layout-inner-fixed-left-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-format-align-left"></i
                      ><span class="hide-menu"> Inner Fixed Left Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="layout-inner-fixed-right-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-format-align-right"></i
                      ><span class="hide-menu"> Inner Fixed Right Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="layout-inner-left-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-format-float-left"></i
                      ><span class="hide-menu"> Inner Left Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="layout-inner-right-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-format-float-right"></i
                      ><span class="hide-menu"> Inner Right Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="page-layout-fixed-header.php" class="sidebar-link"
                      ><i class="mdi mdi-view-quilt"></i
                      ><span class="hide-menu"> Fixed Header </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="page-layout-fixed-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-parallel"></i
                      ><span class="hide-menu"> Fixed Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="page-layout-fixed-header-sidebar.php" class="sidebar-link"
                      ><i class="mdi mdi-view-column"></i
                      ><span class="hide-menu"> Fixed Header &amp; Sidebar </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="page-layout-boxed-layout.php" class="sidebar-link"
                      ><i class="mdi mdi-view-carousel"></i
                      ><span class="hide-menu"> Box Layout </span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Apps</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-chats.php"
                  aria-expanded="false"
                  ><i data-feather="message-circle" class="feather-icon"></i
                  ><span class="hide-menu">Chat Apps</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-todo.php"
                  aria-expanded="false"
                  ><i data-feather="check-circle" class="feather-icon"></i
                  ><span class="hide-menu">Todo</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-calendar.php"
                  aria-expanded="false"
                  ><i data-feather="calendar" class="feather-icon"></i
                  ><span class="hide-menu">Calendar</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-taskboard.php"
                  aria-expanded="false"
                  ><i data-feather="layout" class="feather-icon"></i
                  ><span class="hide-menu">Taskboard</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-contacts.php"
                  aria-expanded="false"
                  ><i data-feather="phone" class="feather-icon"></i
                  ><span class="hide-menu">Contact</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-notes.php"
                  aria-expanded="false"
                  ><i data-feather="book" class="feather-icon"></i
                  ><span class="hide-menu">Notes</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-invoice.php"
                  aria-expanded="false"
                  ><i data-feather="file-text" class="feather-icon"></i
                  ><span class="hide-menu">Invoice</span></a
                >
              </li>

              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="mail" class="feather-icon"></i
                  ><span class="hide-menu">Inbox </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="inbox-email.php" class="sidebar-link"
                      ><i class="mdi mdi-email"></i><span class="hide-menu"> Email </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="inbox-email-detail.php" class="sidebar-link"
                      ><i class="mdi mdi-email-alert"></i
                      ><span class="hide-menu"> Email Detail </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="inbox-email-compose.php" class="sidebar-link"
                      ><i class="mdi mdi-email-secure"></i
                      ><span class="hide-menu"> Email Compose </span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="bookmark" class="feather-icon"></i
                  ><span class="hide-menu">Ticket </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="ticket-list.php" class="sidebar-link"
                      ><i class="mdi mdi-book-multiple"></i
                      ><span class="hide-menu"> Ticket List </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ticket-detail.php" class="sidebar-link"
                      ><i class="mdi mdi-book-plus"></i
                      ><span class="hide-menu"> Ticket Detail </span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">UI</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="cpu" class="feather-icon"></i
                  ><span class="hide-menu">Ui Elements </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="ui-accordian.php" class="sidebar-link"
                      ><i class="mdi mdi-box-shadow"></i
                      ><span class="hide-menu"> Accordian</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-badge.php" class="sidebar-link"
                      ><i class="mdi mdi-application"></i><span class="hide-menu"> Badge</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-buttons.php" class="sidebar-link"
                      ><i class="mdi mdi-toggle-switch"></i
                      ><span class="hide-menu"> Buttons</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-dropdowns.php" class="sidebar-link"
                      ><i class="mdi mdi-arrange-bring-to-front"></i
                      ><span class="hide-menu"> Dropdowns</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-modals.php" class="sidebar-link"
                      ><i class="mdi mdi-tablet"></i><span class="hide-menu"> Modals</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-tab.php" class="sidebar-link"
                      ><i class="mdi mdi-sort-variant"></i><span class="hide-menu"> Tab</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-tooltip-popover.php" class="sidebar-link"
                      ><i class="mdi mdi-image-filter-vintage"></i
                      ><span class="hide-menu"> Tooltip &amp; Popover</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-notification.php" class="sidebar-link"
                      ><i class="mdi mdi-message-bulleted"></i
                      ><span class="hide-menu"> Notification</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-progressbar.php" class="sidebar-link"
                      ><i class="mdi mdi-poll"></i><span class="hide-menu"> Progressbar</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-pagination.php" class="sidebar-link"
                      ><i class="mdi mdi-altimeter"></i
                      ><span class="hide-menu"> Pagination</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-typography.php" class="sidebar-link"
                      ><i class="mdi mdi-format-line-spacing"></i
                      ><span class="hide-menu"> Typography</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-bootstrap.php" class="sidebar-link"
                      ><i class="mdi mdi-bootstrap"></i
                      ><span class="hide-menu"> Bootstrap Ui</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-breadcrumb.php" class="sidebar-link"
                      ><i class="mdi mdi-equal"></i><span class="hide-menu"> Breadcrumb</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-offcanvas.php" class="sidebar-link"
                      ><i class="mdi mdi-content-copy"></i
                      ><span class="hide-menu"> Offcanvas</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-lists.php" class="sidebar-link"
                      ><i class="mdi mdi-file-video"></i><span class="hide-menu"> Lists</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-grid.php" class="sidebar-link"
                      ><i class="mdi mdi-view-module"></i><span class="hide-menu"> Grid</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-carousel.php" class="sidebar-link"
                      ><i class="mdi mdi-view-carousel"></i
                      ><span class="hide-menu"> Carousel</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-scrollspy.php" class="sidebar-link"
                      ><i class="mdi mdi-application"></i
                      ><span class="hide-menu"> Scrollspy</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-toasts.php" class="sidebar-link"
                      ><i class="mdi mdi-credit-card-scan"></i
                      ><span class="hide-menu"> Toasts</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-spinner.php" class="sidebar-link"
                      ><i class="mdi mdi-apple-safari"></i
                      ><span class="hide-menu"> Spinner</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="copy" class="feather-icon"></i
                  ><span class="hide-menu">Cards</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="ui-cards.php" class="sidebar-link"
                      ><i class="mdi mdi-layers"></i><span class="hide-menu"> Basic Cards</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-card-customs.php" class="sidebar-link"
                      ><i class="mdi mdi-credit-card-scan"></i
                      ><span class="hide-menu">Custom Cards</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-card-weather.php" class="sidebar-link"
                      ><i class="mdi mdi-weather-fog"></i
                      ><span class="hide-menu">Weather Cards</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-card-draggable.php" class="sidebar-link"
                      ><i class="mdi mdi-bandcamp"></i
                      ><span class="hide-menu">Draggable Cards</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="hard-drive" class="feather-icon"></i
                  ><span class="hide-menu">Components</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="component-sweetalert.php" class="sidebar-link"
                      ><i class="mdi mdi-layers"></i><span class="hide-menu"> Sweet Alert</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="component-nestable.php" class="sidebar-link"
                      ><i class="mdi mdi-credit-card-scan"></i
                      ><span class="hide-menu">Nestable</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="component-noui-slider.php" class="sidebar-link"
                      ><i class="mdi mdi-weather-fog"></i
                      ><span class="hide-menu">Noui slider</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="component-rating.php" class="sidebar-link"
                      ><i class="mdi mdi-bandcamp"></i><span class="hide-menu">Rating</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="component-toastr.php" class="sidebar-link"
                      ><i class="mdi mdi-poll"></i><span class="hide-menu">Toastr</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="grid" class="feather-icon"></i
                  ><span class="hide-menu">Widgets </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="widgets-feeds.php" class="sidebar-link"
                      ><i class="mdi mdi-account-box"></i
                      ><span class="hide-menu"> Feed Widgets </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="widgets-apps.php" class="sidebar-link"
                      ><i class="mdi mdi-comment-processing-outline"></i
                      ><span class="hide-menu"> Apps Widgets </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="widgets-data.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar"></i
                      ><span class="hide-menu"> Data Widgets </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="widgets-charts.php" class="sidebar-link"
                      ><i class="mdi mdi-bulletin-board"></i
                      ><span class="hide-menu"> Charts Widgets</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Forms</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="layers" class="feather-icon"></i
                  ><span class="hide-menu">Form Elements</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-inputs.php" class="sidebar-link"
                      ><i class="mdi mdi-priority-low"></i
                      ><span class="hide-menu"> Forms Input</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-input-groups.php" class="sidebar-link"
                      ><i class="mdi mdi-rounded-corner"></i
                      ><span class="hide-menu"> Input Groups</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-input-grid.php" class="sidebar-link"
                      ><i class="mdi mdi-select-all"></i
                      ><span class="hide-menu"> Input Grid</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-checkbox-radio.php" class="sidebar-link"
                      ><i class="mdi mdi-shape-plus"></i
                      ><span class="hide-menu"> Checkboxes &amp; Radios</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-bootstrap-touchspin.php" class="sidebar-link"
                      ><i class="mdi mdi-switch"></i
                      ><span class="hide-menu"> Bootstrap Touchspin</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-bootstrap-switch.php" class="sidebar-link"
                      ><i class="mdi mdi-toggle-switch-off"></i
                      ><span class="hide-menu"> Bootstrap Switch</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-select2.php" class="sidebar-link"
                      ><i class="mdi mdi-relative-scale"></i
                      ><span class="hide-menu"> Select2</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-dual-listbox.php" class="sidebar-link"
                      ><i class="mdi mdi-tab-unselected"></i
                      ><span class="hide-menu"> Dual Listbox</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="file-text" class="feather-icon"></i
                  ><span class="hide-menu">Form Layouts</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-vector-difference-ba"></i
                      ><span class="hide-menu"> Basic Forms</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-horizontal.php" class="sidebar-link"
                      ><i class="mdi mdi-file-document-box"></i
                      ><span class="hide-menu"> Form Horizontal</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-actions.php" class="sidebar-link"
                      ><i class="mdi mdi-code-greater-than"></i
                      ><span class="hide-menu"> Form Actions</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-row-separator.php" class="sidebar-link"
                      ><i class="mdi mdi-code-equal"></i
                      ><span class="hide-menu"> Row Separator</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-bordered.php" class="sidebar-link"
                      ><i class="mdi mdi-flip-to-front"></i
                      ><span class="hide-menu"> Form Bordered</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-striped-row.php" class="sidebar-link"
                      ><i class="mdi mdi-content-duplicate"></i
                      ><span class="hide-menu"> Striped Rows</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-detail.php" class="sidebar-link"
                      ><i class="mdi mdi-cards-outline"></i
                      ><span class="hide-menu"> Form Detail</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="package" class="feather-icon"></i
                  ><span class="hide-menu">Form Addons</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-paginator.php" class="sidebar-link"
                      ><i class="mdi mdi-export"></i><span class="hide-menu"> Paginator</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-img-cropper.php" class="sidebar-link"
                      ><i class="mdi mdi-crop"></i><span class="hide-menu"> Image Cropper</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-dropzone.php" class="sidebar-link"
                      ><i class="mdi mdi-crosshairs-gps"></i
                      ><span class="hide-menu"> Dropzone</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-mask.php" class="sidebar-link"
                      ><i class="mdi mdi-box-shadow"></i
                      ><span class="hide-menu"> Form Mask</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-typeahead.php" class="sidebar-link"
                      ><i class="mdi mdi-cards-variant"></i
                      ><span class="hide-menu"> Form Typehead</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-custom-switch.php" class="sidebar-link"
                      ><i class="mdi mdi-toggle-switch-off"></i
                      ><span class="hide-menu"> Custom Switch</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="check-square" class="feather-icon"></i
                  ><span class="hide-menu">Form Validation</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-bootstrap-validation.php" class="sidebar-link"
                      ><i class="mdi mdi-credit-card-scan"></i
                      ><span class="hide-menu"> Bootstrap Validation</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-custom-validation.php" class="sidebar-link"
                      ><i class="mdi mdi-credit-card-plus"></i
                      ><span class="hide-menu"> Custom Validation</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="droplet" class="feather-icon"></i
                  ><span class="hide-menu">Form Pickers</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-picker-colorpicker.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar-plus"></i
                      ><span class="hide-menu"> Form Colorpicker</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-picker-datetimepicker.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar-clock"></i
                      ><span class="hide-menu"> Form Datetimepicker</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-picker-bootstrap-rangepicker.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar-range"></i
                      ><span class="hide-menu"> Form Bootstrap Rangepicker</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-picker-bootstrap-datepicker.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar-check"></i
                      ><span class="hide-menu"> Form Bootstrap Datepicker</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-picker-material-datepicker.php" class="sidebar-link"
                      ><i class="mdi mdi-calendar-text"></i
                      ><span class="hide-menu"> Form Material Datepicker</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="edit" class="feather-icon"></i
                  ><span class="hide-menu">Form Editor</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="form-editor-ckeditor.php" class="sidebar-link"
                      ><i class="mdi mdi-drawing"></i><span class="hide-menu">Ck Editor</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-editor-quill.php" class="sidebar-link"
                      ><i class="mdi mdi-drupal"></i><span class="hide-menu">Quill Editor</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-editor-summernote.php" class="sidebar-link"
                      ><i class="mdi mdi-brightness-6"></i
                      ><span class="hide-menu">Summernote Editor</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="form-editor-tinymce.php" class="sidebar-link"
                      ><i class="mdi mdi-bowling"></i
                      ><span class="hide-menu">Tinymce Edtor</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="form-wizard.php"
                  aria-expanded="false"
                  ><i data-feather="credit-card" class="feather-icon"></i
                  ><span class="hide-menu">Form Wizard</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="form-repeater.php"
                  aria-expanded="false"
                  ><i data-feather="crop" class="feather-icon"></i
                  ><span class="hide-menu">Form Repeater</span></a
                >
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Tables</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="codepen" class="feather-icon"></i
                  ><span class="hide-menu">Bootstrap Tables</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="table-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-border-all"></i
                      ><span class="hide-menu">Basic Table </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="table-dark-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-border-left"></i
                      ><span class="hide-menu">Dark Basic Table </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="table-sizing.php" class="sidebar-link"
                      ><i class="mdi mdi-border-outside"></i
                      ><span class="hide-menu">Sizing Table </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="table-layout-coloured.php" class="sidebar-link"
                      ><i class="mdi mdi-border-bottom"></i
                      ><span class="hide-menu">Coloured Table Layout</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="hard-drive" class="feather-icon"></i
                  ><span class="hide-menu">Datatables</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="table-datatable-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-border-vertical"></i
                      ><span class="hide-menu"> Basic Initialisation</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="table-datatable-api.php" class="sidebar-link"
                      ><i class="mdi mdi-blur-linear"></i><span class="hide-menu"> API</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="table-datatable-advanced.php" class="sidebar-link"
                      ><i class="mdi mdi-border-style"></i
                      ><span class="hide-menu"> Advanced Initialisation</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="table-jsgrid.php"
                  aria-expanded="false"
                  ><i data-feather="disc" class="feather-icon"></i
                  ><span class="hide-menu">Table Jsgrid</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="table-responsive.php"
                  aria-expanded="false"
                  ><i data-feather="smartphone" class="feather-icon"></i
                  ><span class="hide-menu">Table Responsive</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="table-footable.php"
                  aria-expanded="false"
                  ><i data-feather="command" class="feather-icon"></i
                  ><span class="hide-menu">Table Footable</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="table-editable.php"
                  aria-expanded="false"
                  ><i data-feather="edit-2" class="feather-icon"></i
                  ><span class="hide-menu">Table Editable</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="table-bootstrap.php"
                  aria-expanded="false"
                  ><i data-feather="target" class="feather-icon"></i
                  ><span class="hide-menu">Table Bootstrap</span></a
                >
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Charts</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="chart-morris.php"
                  aria-expanded="false"
                  ><i data-feather="loader" class="feather-icon"></i
                  ><span class="hide-menu"> Morris Chart</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="chart-chart-js.php"
                  aria-expanded="false"
                  ><i data-feather="pie-chart" class="feather-icon"></i
                  ><span class="hide-menu">Chartjs</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="chart-sparkline.php"
                  aria-expanded="false"
                  ><i data-feather="radio" class="feather-icon"></i
                  ><span class="hide-menu">Sparkline Chart</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="chart-chartist.php"
                  aria-expanded="false"
                  ><i data-feather="trello" class="feather-icon"></i
                  ><span class="hide-menu">Chartist Chart</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="slack" class="feather-icon"></i
                  ><span class="hide-menu">C3 Charts</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="chart-c3-axis.php" class="sidebar-link"
                      ><i class="mdi mdi-arrange-bring-to-front"></i>
                      <span class="hide-menu">Axis Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-c3-bar.php" class="sidebar-link"
                      ><i class="mdi mdi-arrange-send-to-back"></i>
                      <span class="hide-menu">Bar Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-c3-data.php" class="sidebar-link"
                      ><i class="mdi mdi-backup-restore"></i>
                      <span class="hide-menu">Data Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-c3-line.php" class="sidebar-link"
                      ><i class="mdi mdi-backburger"></i>
                      <span class="hide-menu">Line Chart</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="javascript:void(0)"
                  aria-expanded="false"
                  ><i data-feather="life-buoy" class="feather-icon"></i
                  ><span class="hide-menu">Apex Charts</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="chart-apex-line.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-line"></i>
                      <span class="hide-menu">Line Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-apex-area.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-areaspline"></i>
                      <span class="hide-menu">Area Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-apex-bar.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-gantt"></i>
                      <span class="hide-menu">Bar Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-apex-pie.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-pie"></i> <span class="hide-menu">Pie Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-apex-radial.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-arc"></i>
                      <span class="hide-menu">Radial Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-apex-radar.php" class="sidebar-link"
                      ><i class="mdi mdi-hexagon-outline"></i>
                      <span class="hide-menu">Radar Chart</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="bar-chart-2" class="feather-icon"></i
                  ><span class="hide-menu">Echarts</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="chart-echart-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-line"></i>
                      <span class="hide-menu">Basic Charts</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-echart-bar.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-scatterplot-hexbin"></i>
                      <span class="hide-menu">Bar Chart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="chart-echart-pie-doughnut.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-pie"></i>
                      <span class="hide-menu">Pie &amp; Doughnut Chart</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Sample Pages</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="shopping-cart" class="feather-icon"></i
                  ><span class="hide-menu">Ecommerce Pages</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="eco-products.php" class="sidebar-link"
                      ><i class="mdi mdi-cards-variant"></i>
                      <span class="hide-menu">Products</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="eco-products-cart.php" class="sidebar-link"
                      ><i class="mdi mdi-cart"></i> <span class="hide-menu">Products Cart</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="eco-products-edit.php" class="sidebar-link"
                      ><i class="mdi mdi-cart-plus"></i>
                      <span class="hide-menu">Products Edit</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="eco-products-detail.php" class="sidebar-link"
                      ><i class="mdi mdi-camera-burst"></i>
                      <span class="hide-menu">Product Details</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="eco-products-orders.php" class="sidebar-link"
                      ><i class="mdi mdi-chart-pie"></i>
                      <span class="hide-menu">Product Orders</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="eco-products-checkout.php" class="sidebar-link"
                      ><i class="mdi mdi-clipboard-check"></i>
                      <span class="hide-menu">Products Checkout</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="book-open" class="feather-icon"></i
                  ><span class="hide-menu">Sample Pages </span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="starter-kit.php" class="sidebar-link"
                      ><i class="mdi mdi-crop-free"></i>
                      <span class="hide-menu">Starter Kit</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-animation.php" class="sidebar-link"
                      ><i class="mdi mdi-debug-step-over"></i>
                      <span class="hide-menu">Animation</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-search-result.php" class="sidebar-link"
                      ><i class="mdi mdi-search-web"></i>
                      <span class="hide-menu">Search Result</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-gallery.php" class="sidebar-link"
                      ><i class="mdi mdi-camera-iris"></i> <span class="hide-menu">Gallery</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-treeview.php" class="sidebar-link"
                      ><i class="mdi mdi-file-tree"></i> <span class="hide-menu">Treeview</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-block-ui.php" class="sidebar-link"
                      ><i class="mdi mdi-codepen"></i> <span class="hide-menu">Block UI</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-session-timeout.php" class="sidebar-link"
                      ><i class="mdi mdi-timer-off"></i>
                      <span class="hide-menu">Session Timeout</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-session-idle-timeout.php" class="sidebar-link"
                      ><i class="mdi mdi-timer-sand-empty"></i>
                      <span class="hide-menu">Session Idle Timeout</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-utility-classes.php" class="sidebar-link"
                      ><i class="mdi mdi-tune"></i> <span class="hide-menu">Helper Classes</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-maintenance.php" class="sidebar-link"
                      ><i class="mdi mdi-camera-iris"></i>
                      <span class="hide-menu">Maintenance Page</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="lock" class="feather-icon"></i
                  ><span class="hide-menu">Authentication</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="authentication-login1.php" class="sidebar-link"
                      ><i class="mdi mdi-account-key"></i><span class="hide-menu"> Login </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-login2.php" class="sidebar-link"
                      ><i class="mdi mdi-account-key"></i
                      ><span class="hide-menu"> Login 2 </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-login3.php" class="sidebar-link"
                      ><i class="mdi mdi-account-key"></i
                      ><span class="hide-menu"> Login 3 </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-login3.php" class="sidebar-link"
                      ><i class="mdi mdi-account-key"></i
                      ><span class="hide-menu"> Login 3 </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-register1.php" class="sidebar-link"
                      ><i class="mdi mdi-account-plus"></i
                      ><span class="hide-menu"> Register</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-register2.php" class="sidebar-link"
                      ><i class="mdi mdi-account-plus"></i
                      ><span class="hide-menu"> Register 2</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-lockscreen.php" class="sidebar-link"
                      ><i class="mdi mdi-account-off"></i
                      ><span class="hide-menu"> Lockscreen</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="authentication-recover-password.php" class="sidebar-link"
                      ><i class="mdi mdi-account-convert"></i
                      ><span class="hide-menu"> Recover password</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="users" class="feather-icon"></i
                  ><span class="hide-menu">Users</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="ui-user-card.php" class="sidebar-link"
                      ><i class="mdi mdi-account-box"></i>
                      <span class="hide-menu"> User Card </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-profile.php" class="sidebar-link"
                      ><i class="mdi mdi-account-network"></i
                      ><span class="hide-menu"> User Profile</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="ui-user-contacts.php" class="sidebar-link"
                      ><i class="mdi mdi-account-star-variant"></i
                      ><span class="hide-menu"> User Contact</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="paperclip" class="feather-icon"></i
                  ><span class="hide-menu">Invoice</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="pages-invoice.php" class="sidebar-link"
                      ><i class="mdi mdi-vector-triangle"></i
                      ><span class="hide-menu"> Invoice Layout </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="pages-invoice-list.php" class="sidebar-link"
                      ><i class="mdi mdi-vector-rectangle"></i
                      ><span class="hide-menu"> Invoice List</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="map" class="feather-icon"></i
                  ><span class="hide-menu">Maps</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="map-google.php" class="sidebar-link"
                      ><i class="mdi mdi-google-maps"></i
                      ><span class="hide-menu"> Google Map </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="map-vector.php" class="sidebar-link"
                      ><i class="mdi mdi-map-marker-radius"></i
                      ><span class="hide-menu"> Vector Map</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="feather" class="feather-icon"></i
                  ><span class="hide-menu">Icons</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="icon-fontawesome.php" class="sidebar-link"
                      ><i class="mdi mdi-emoticon-cool"></i
                      ><span class="hide-menu"> Fontawesome Icons</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="icon-feather.php" class="sidebar-link"
                      ><i class="mdi mdi-feather"></i>
                      <span class="hide-menu"> Feather Icons </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="icon-bootstrap.php" class="sidebar-link"
                      ><i class="mdi mdi-emoticon"></i>
                      <span class="hide-menu"> Bootstrap Icons </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="icon-remix.php" class="sidebar-link"
                      ><i class="mdi mdi-emoticon-cool"></i
                      ><span class="hide-menu"> Remix Icons</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="activity" class="feather-icon"></i
                  ><span class="hide-menu">Timeline</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="timeline-center.php" class="sidebar-link"
                      ><i class="mdi mdi-clock-fast"></i>
                      <span class="hide-menu"> Center Timeline </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="timeline-horizontal.php" class="sidebar-link"
                      ><i class="mdi mdi-clock-end"></i
                      ><span class="hide-menu"> Horizontal Timeline</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="timeline-left.php" class="sidebar-link"
                      ><i class="mdi mdi-clock-in"></i
                      ><span class="hide-menu"> Left Timeline</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="timeline-right.php" class="sidebar-link"
                      ><i class="mdi mdi-clock-start"></i
                      ><span class="hide-menu"> Right Timeline</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="mail" class="feather-icon"></i
                  ><span class="hide-menu">Email Template</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="email-templete-alert.php" class="sidebar-link"
                      ><i class="mdi mdi-message-alert"></i>
                      <span class="hide-menu"> Alert </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="email-templete-basic.php" class="sidebar-link"
                      ><i class="mdi mdi-message-bulleted"></i
                      ><span class="hide-menu"> Basic</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="email-templete-billing.php" class="sidebar-link"
                      ><i class="mdi mdi-message-draw"></i
                      ><span class="hide-menu"> Billing</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="email-templete-password-reset.php" class="sidebar-link"
                      ><i class="mdi mdi-message-bulleted-off"></i
                      ><span class="hide-menu"> Password-Reset</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="user-x" class="feather-icon"></i
                  ><span class="hide-menu">Error Pages</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="error-400.php" class="sidebar-link"
                      ><i class="mdi mdi-alert-outline"></i>
                      <span class="hide-menu"> Error 400 </span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="error-403.php" class="sidebar-link"
                      ><i class="mdi mdi-alert-outline"></i
                      ><span class="hide-menu"> Error 403</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="error-404.php" class="sidebar-link"
                      ><i class="mdi mdi-alert-outline"></i
                      ><span class="hide-menu"> Error 404</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="error-500.php" class="sidebar-link"
                      ><i class="mdi mdi-alert-outline"></i
                      ><span class="hide-menu"> Error 500</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="error-503.php" class="sidebar-link"
                      ><i class="mdi mdi-alert-outline"></i
                      ><span class="hide-menu"> Error 503</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link has-arrow waves-effect waves-dark"
                  href="#"
                  aria-expanded="false"
                  ><i data-feather="git-pull-request" class="feather-icon"></i
                  ><span class="hide-menu">Multi level dd</span></a
                >
                <ul aria-expanded="false" class="collapse first-level">
                  <li class="sidebar-item">
                    <a href="#" class="sidebar-link"
                      ><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.1</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a href="#" class="sidebar-link"
                      ><i class="mdi mdi-octagram"></i><span class="hide-menu"> item 1.2</span></a
                    >
                  </li>
                  <li class="sidebar-item">
                    <a class="has-arrow sidebar-link" href="#" aria-expanded="false"
                      ><i class="mdi mdi-playlist-plus"></i>
                      <span class="hide-menu">Menu 1.3</span></a
                    >
                    <ul aria-expanded="false" class="collapse second-level">
                      <li class="sidebar-item">
                        <a href="#" class="sidebar-link"
                          ><i class="mdi mdi-octagram"></i
                          ><span class="hide-menu"> item 1.3.1</span></a
                        >
                      </li>
                      <li class="sidebar-item">
                        <a href="#" class="sidebar-link"
                          ><i class="mdi mdi-octagram"></i
                          ><span class="hide-menu"> item 1.3.2</span></a
                        >
                      </li>
                      <li class="sidebar-item">
                        <a href="#" class="sidebar-link"
                          ><i class="mdi mdi-octagram"></i
                          ><span class="hide-menu"> item 1.3.3</span></a
                        >
                      </li>
                      <li class="sidebar-item">
                        <a href="#" class="sidebar-link"
                          ><i class="mdi mdi-octagram"></i
                          ><span class="hide-menu"> item 1.3.4</span></a
                        >
                      </li>
                    </ul>
                  </li>
                  <li class="sidebar-item">
                    <a href="#" class="sidebar-link"
                      ><i class="mdi mdi-playlist-check"></i
                      ><span class="hide-menu"> item 1.4</span></a
                    >
                  </li>
                </ul>
              </li>
              <li class="nav-small-cap">
                <i class="mdi mdi-dots-horizontal"></i>
                <span class="hide-menu">Extra</span>
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="docs/documentation.php"
                  aria-expanded="false"
                  ><i data-feather="edit-3" class="feather-icon"></i
                  ><span class="hide-menu">Documentation</span></a
                >
              </li>
              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="authentication-login1.php"
                  aria-expanded="false"
                  ><i data-feather="log-out" class="feather-icon"></i
                  ><span class="hide-menu">Log Out</span></a
                >
              </li>
            </ul>
          </nav>
          <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
      </aside>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">Dashboard</h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                  </ol>
                </nav>
              </div>
            </div>
            <div class="col-7 align-self-center">
              <div class="d-flex no-block justify-content-end align-items-center">
                <div class="me-2">
                  <div class="lastmonth"></div>
                </div>
                <div class="">
                  <small>LAST MONTH</small>
                  <h4 class="text-info mb-0 font-medium">$58,256</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Sales chart -->
          <!-- -------------------------------------------------------------- -->
          <div class="row">
            <div class="col-12">
              <!-- ---------------------
                            start Sales Summary
                        ---------------- -->
              <div class="card">
                <div class="card-body">
                  <div class="d-md-flex align-items-center">
                    <div>
                      <h4 class="card-title">Sales Summary</h4>
                      <h5 class="card-subtitle">Overview of Latest Month</h5>
                    </div>
                    <div class="ms-auto d-flex no-block align-items-center">
                      <ul class="list-inline fs-2 dl me-3 mb-0">
                        <li class="list-inline-item text-info">
                          <i class="ri-checkbox-blank-circle-fill align-middle fs-3"></i>
                          Iphone
                        </li>
                        <li class="list-inline-item text-primary">
                          <i class="ri-checkbox-blank-circle-fill align-middle fs-3"></i>
                          Ipad
                        </li>
                      </ul>
                      <div class="dl">
                        <select class="form-select">
                          <option value="0" selected>Monthly</option>
                          <option value="1">Daily</option>
                          <option value="2">Weekly</option>
                          <option value="3">Yearly</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <!-- column -->
                    <div class="col-lg-4 d-md-flex d-lg-block justify-content-between">
                      <div>
                        <h1 class="mb-0 mt-4">$6,890.68</h1>
                        <h6 class="fw-light text-muted">Current Month Earnings</h6>
                      </div>
                      <div>
                        <h3 class="mt-4 mb-0">1,540</h3>
                        <h6 class="fw-light text-muted mb-md-0 mt-md-2 mt-lg-0 mt-0">
                          Current Month Sales
                        </h6>
                      </div>
                      <a class="btn btn-info mt-3 p-3 pl-4 pr-4 mb-3" href="javascript:void(0)"
                        >Last Month Summary</a
                      >
                    </div>
                    <!-- column (sales summery chart) -->
                    <div class="col-lg-8">
                      <div class="sales-summery"></div>
                    </div>
                    <!-- column -->
                  </div>
                </div>
                <!-- -------------------------------------------------------------- -->
                <!-- Wallet  Summary-->
                <!-- -------------------------------------------------------------- -->
                <div class="card-body border-top">
                  <div class="row mb-0">
                    <!-- col -->
                    <div class="col-lg-3 col-md-6 mb-3 mb-lg-0">
                      <div class="d-flex align-items-center">
                        <div class="me-2">
                          <span class="text-orange display-5"
                            ><i class="ri-wallet-2-fill"></i
                          ></span>
                        </div>
                        <div>
                          <span>Wallet Balance</span>
                          <h3 class="font-medium mb-0">$3,567.53</h3>
                        </div>
                      </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6 mb-3 mb-lg-0">
                      <div class="d-flex align-items-center">
                        <div class="me-2">
                          <span class="text-cyan display-5"
                            ><i class="ri-money-cny-circle-fill"></i
                          ></span>
                        </div>
                        <div>
                          <span>Referral Earnings</span>
                          <h3 class="font-medium mb-0">$769.08</h3>
                        </div>
                      </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6 mb-3 mb-md-0">
                      <div class="d-flex align-items-center">
                        <div class="me-2">
                          <span class="text-info display-5"
                            ><i class="ri-shopping-bag-fill"></i
                          ></span>
                        </div>
                        <div>
                          <span>Estimate Sales</span>
                          <h3 class="font-medium mb-0">5489</h3>
                        </div>
                      </div>
                    </div>
                    <!-- col -->
                    <!-- col -->
                    <div class="col-lg-3 col-md-6">
                      <div class="d-flex align-items-center">
                        <div class="me-2">
                          <span class="text-primary display-5"
                            ><i data-feather="dollar-sign"></i
                          ></span>
                        </div>
                        <div>
                          <span>Earnings</span>
                          <h3 class="font-medium mb-0">$23,568.90</h3>
                        </div>
                      </div>
                    </div>
                    <!-- col -->
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Sales Summary
                        ---------------- -->
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Sales chart -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Email campaign chart -->
          <!-- -------------------------------------------------------------- -->
          <div class="row">
            <div class="col-lg-8 col-xl-6 d-flex align-items-stretch">
              <!-- ---------------------
                            start Email Campaigns
                        ---------------- -->
              <div class="card card-hover w-100">
                <div class="card-body">
                  <div class="d-md-flex align-items-center">
                    <div>
                      <h4 class="card-title">Email Campaigns</h4>
                      <h5 class="card-subtitle">Overview of Email Campaigns</h5>
                    </div>
                    <div class="ms-auto align-items-center">
                      <div class="dl">
                        <select class="form-select">
                          <option value="0" selected>Monthly</option>
                          <option value="1">Daily</option>
                          <option value="2">Weekly</option>
                          <option value="3">Yearly</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <!-- column -->
                  <div class="row mt-5">
                    <!-- column -->
                    <div class="col-lg-6 d-flex justify-content-center align-items-center">
                      <div id="email-campaign"></div>
                    </div>
                    <!-- column (Open Ratio for Campaigns chart) -->
                    <div class="col-lg-6">
                      <h1 class="display-6 mb-0 font-medium">45%</h1>
                      <span>Open Ratio for Campaigns</span>
                      <ul class="list-style-none">
                        <li class="mt-3 d-flex align-items-center">
                          <i class="ri-checkbox-blank-circle-fill fs-3 me-2 text-muted fs-2"></i>
                          Open Ratio <span class="ms-auto">45%</span>
                        </li>
                        <li class="mt-3 d-flex align-items-center">
                          <i class="ri-checkbox-blank-circle-fill fs-3 me-1 text-info fs-2"></i>
                          Clicked Ratio <span class="ms-auto">14%</span>
                        </li>
                        <li class="mt-3 d-flex align-items-center">
                          <i class="ri-checkbox-blank-circle-fill fs-3 me-1 text-orange fs-2"></i>
                          Un-Open Ratio <span class="ms-auto">25%</span>
                        </li>
                        <li class="mt-3 d-flex align-items-center">
                          <i class="ri-checkbox-blank-circle-fill fs-3 me-1 text-primary fs-2"></i>
                          Bounced Ratio <span class="ms-auto">17%</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- column -->
                </div>
              </div>
              <!-- ---------------------
                            start Email Campaigns
                        ---------------- -->
            </div>
            <div class="col-lg-4 col-xl-6 d-flex align-items-stretch">
              <!-- ---------------------
                            start Active Visitors on Site
                        ---------------- -->
              <div class="card card-hover w-100">
                <div
                  class="card-body"
                  style="
                    background: url(assets/images/active-bg.png) no-repeat top
                      center;
                  "
                >
                  <div class="pt-3 text-center">
                    <i class="ri-file-text-fill display-4 text-orange d-block"></i>
                    <span class="display-4 d-block font-medium">368</span>
                    <span>Active Visitors on Site</span>
                    <!-- Progress -->
                    <div class="progress mt-5" style="height: 4px">
                      <div
                        class="progress-bar bg-info"
                        role="progressbar"
                        style="width: 15%"
                        aria-valuenow="15"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                      <div
                        class="progress-bar bg-orange"
                        role="progressbar"
                        style="width: 30%"
                        aria-valuenow="30"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                      <div
                        class="progress-bar bg-warning"
                        role="progressbar"
                        style="width: 65%"
                        aria-valuenow="20"
                        aria-valuemin="0"
                        aria-valuemax="100"
                      ></div>
                    </div>
                    <!-- Progress -->
                    <!-- row -->
                    <div class="row mt-4 mb-3">
                      <!-- column -->
                      <div class="col-4 border-end text-start">
                        <h3 class="mb-0 font-medium">60%</h3>
                        Desktop
                      </div>
                      <!-- column -->
                      <div class="col-4 border-end">
                        <h3 class="mb-0 font-medium">28%</h3>
                        Mobile
                      </div>
                      <!-- column -->
                      <div class="col-4 text-end">
                        <h3 class="mb-0 font-medium">12%</h3>
                        Tablet
                      </div>
                    </div>
                    <a
                      href="javascript:void(0)"
                      class="waves-effect waves-light mt-3 btn btn-lg btn-info accent-4 mb-3"
                      >View More Details</a
                    >
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Active Visitors on Site
                        ---------------- -->
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Email campaign chart -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Ravenue - page-view-bounce rate -->
          <!-- -------------------------------------------------------------- -->
          <div class="row">
            <!-- column -->
            <div class="col-lg-4 d-flex align-items-stretch">
              <!-- ---------------------
                            start Revenue Statistics
                        ---------------- -->
              <div class="card bg-info text-white card-hover w-100">
                <div class="card-body">
                  <h4 class="card-title">Revenue Statistics</h4>
                  <div class="d-flex align-items-center mt-4">
                    <div class="" id="ravenue"></div>
                    <div class="ms-auto">
                      <h3 class="font-medium white-text mb-0">$351</h3>
                      <span class="white-text op-5">Jan 10 - Jan 20</span>
                    </div>
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Revenue Statistics
                        ---------------- -->
            </div>
            <!-- column -->
            <div class="col-lg-4 d-flex align-items-stretch">
              <!-- ---------------------
                            start Page Views
                        ---------------- -->
              <div class="card bg-cyan text-white card-hover w-100">
                <div class="card-body">
                  <h4 class="card-title">Page Views</h4>
                  <h3 class="white-text mb-0"><i class="ri-arrow-up-line"></i> 6548</h3>
                </div>
                <div class="mt-3" id="views"></div>
              </div>
              <!-- ---------------------
                            end Page Views
                        ---------------- -->
            </div>
            <!-- column -->
            <div class="col-lg-4 d-flex align-items-stretch">
              <!-- ---------------------
                            start Bounce Rate
                        ---------------- -->
              <div class="card card-hover w-100">
                <div class="card-body">
                  <h4 class="card-title">Bounce Rate</h4>
                  <div class="d-flex no-block align-items-center mt-4">
                    <div class="">
                      <h3 class="font-medium mb-0">56.33%</h3>
                      <span class="">Total Bounce</span>
                    </div>
                    <div class="ms-auto">
                      <div>
                        <div id="bouncerate"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Bounce Rate
                        ---------------- -->
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Ravenue - page-view-bounce rate -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Table -->
          <!-- -------------------------------------------------------------- -->
          <div class="row">
            <div class="col-lg-12">
              <!-- ---------------------
                            start Projects of the Month
                        ---------------- -->
              <div class="card">
                <div class="card-body">
                  <div class="d-md-flex align-items-center">
                    <div>
                      <h4 class="card-title">Projects of the Month</h4>
                      <h5 class="card-subtitle">Overview of Latest Month</h5>
                    </div>
                    <div class="ms-auto d-flex no-block align-items-center">
                      <div class="dl">
                        <select class="form-select">
                          <option value="0" selected>Monthly</option>
                          <option value="1">Daily</option>
                          <option value="2">Weekly</option>
                          <option value="3">Yearly</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table no-wrap v-middle">
                      <thead>
                        <!-- start row -->
                        <tr class="border-0">
                          <th class="border-0">Team Lead</th>
                          <th class="border-0">Project</th>
                          <th class="border-0">Team</th>
                          <th class="border-0">Status</th>
                          <th class="border-0">Weeks</th>
                          <th class="border-0">Budget</th>
                        </tr>
                        <!-- end row -->
                      </thead>
                      <tbody>
                        <!-- start row -->
                        <tr>
                          <td>
                            <div class="d-flex no-block align-items-center">
                              <div class="me-2">
                                <img
                                  src="assets/images/d1.jpg"
                                  alt="user"
                                  class="rounded-circle"
                                  width="45"
                                />
                              </div>
                              <div class="">
                                <h5 class="mb-0 fs-4 font-medium">Hanna Gover</h5>
                                <span>hgover@gmail.com</span>
                              </div>
                            </div>
                          </td>
                          <td>Elite Admin</td>
                          <td>
                            <div class="popover-icon">
                              <a class="btn-circle btn btn-info" href="javascript:void(0)">SS</a>
                              <a
                                class="btn-circle btn btn-cyan text-white popover-item"
                                href="javascript:void(0)"
                                >DS</a
                              >
                              <a class="btn-circle btn p-0 popover-item" href="javascript:void(0)"
                                ><img
                                  src="assets/images/1.jpg"
                                  class="rounded-circle"
                                  width="39"
                                  alt=""
                              /></a>
                              <a
                                class="btn-circle btn btn-outline-secondary"
                                href="javascript:void(0)"
                                >+</a
                              >
                            </div>
                          </td>
                          <td>
                            <i
                              class="ri-checkbox-blank-circle-fill text-orange fs-4"
                              data-bs-toggle="tooltip"
                              data-bs-placement="top"
                              title="In Progress"
                            ></i>
                          </td>
                          <td>35</td>
                          <td class="blue-grey-text text-darken-4 font-medium">$96K</td>
                        </tr>
                        <!-- end row -->
                        <!-- start row -->
                        <tr>
                          <td>
                            <div class="d-flex no-block align-items-center">
                              <div class="me-2">
                                <img
                                  src="assets/images/d2.jpg"
                                  alt="user"
                                  class="rounded-circle"
                                  width="45"
                                />
                              </div>
                              <div class="">
                                <h5 class="mb-0 fs-4 font-medium">Daniel Kristeen</h5>
                                <span>Kristeen@gmail.com</span>
                              </div>
                            </div>
                          </td>
                          <td>Elite Admin</td>
                          <td>
                            <div class="popover-icon">
                              <a class="btn-circle btn btn-info" href="javascript:void(0)">SS</a>
                              <a
                                class="btn-circle btn btn-primary text-white popover-item"
                                href="javascript:void(0)"
                                >DS</a
                              >
                              <a
                                class="btn-circle btn btn-outline-secondary"
                                href="javascript:void(0)"
                                >+</a
                              >
                            </div>
                          </td>
                          <td>
                            <i
                              class="ri-checkbox-blank-circle-fill text-success fs-4"
                              data-bs-toggle="tooltip"
                              data-bs-placement="top"
                              title="Active"
                            ></i>
                          </td>
                          <td>35</td>
                          <td class="blue-grey-text text-darken-4 font-medium">$96K</td>
                        </tr>
                        <!-- end row -->
                        <!-- start row -->
                        <tr>
                          <td>
                            <div class="d-flex no-block align-items-center">
                              <div class="me-2">
                                <img
                                  src="assets/images/d3.jpg"
                                  alt="user"
                                  class="rounded-circle"
                                  width="45"
                                />
                              </div>
                              <div class="">
                                <h5 class="mb-0 fs-4 font-medium">Julian Josephs</h5>
                                <span>Josephs@gmail.com</span>
                              </div>
                            </div>
                          </td>
                          <td>Elite Admin</td>
                          <td>
                            <div class="popover-icon">
                              <a class="btn-circle btn btn-info" href="javascript:void(0)">SS</a>
                              <a
                                class="btn-circle btn btn-cyan text-white popover-item"
                                href="javascript:void(0)"
                                >DS</a
                              >
                              <a
                                class="btn-circle btn btn-orange text-white popover-item"
                                href="javascript:void(0)"
                                >RP</a
                              >
                              <a
                                class="btn-circle btn btn-outline-secondary"
                                href="javascript:void(0)"
                                >+</a
                              >
                            </div>
                          </td>
                          <td>
                            <i
                              class="ri-checkbox-blank-circle-fill text-success fs-4"
                              data-bs-toggle="tooltip"
                              data-bs-placement="top"
                              title="Active"
                            ></i>
                          </td>
                          <td>35</td>
                          <td class="blue-grey-text text-darken-4 font-medium">$96K</td>
                        </tr>
                        <!-- end row -->
                        <!-- start row -->
                        <tr>
                          <td>
                            <div class="d-flex no-block align-items-center">
                              <div class="me-2">
                                <img
                                  src="assets/images/2.jpg"
                                  alt="user"
                                  class="rounded-circle"
                                  width="45"
                                />
                              </div>
                              <div class="">
                                <h5 class="mb-0 fs-4 font-medium">Jan Petrovic</h5>
                                <span>hgover@gmail.com</span>
                              </div>
                            </div>
                          </td>
                          <td>Elite Admin</td>
                          <td>
                            <div class="popover-icon">
                              <a
                                class="btn-circle btn btn-orange text-white"
                                href="javascript:void(0)"
                                >RP</a
                              >
                              <a
                                class="btn-circle btn btn-outline-secondary"
                                href="javascript:void(0)"
                                >+</a
                              >
                            </div>
                          </td>
                          <td>
                            <i
                              class="ri-checkbox-blank-circle-fill text-orange fs-4"
                              data-bs-toggle="tooltip"
                              data-bs-placement="top"
                              title="In Progress"
                            ></i>
                          </td>
                          <td>35</td>
                          <td class="blue-grey-text text-darken-4 font-medium">$96K</td>
                        </tr>
                        <!-- end row -->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Projects of the Month
                        ---------------- -->
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Table -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Recent comment and chats -->
          <!-- -------------------------------------------------------------- -->
          <div class="row">
            <!-- column -->
            <div class="col-lg-6">
              <!-- ---------------------
                            start Recent Comments
                        ---------------- -->
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Recent Comments</h4>
                </div>
                <div class="comment-widgets scrollable" style="height: 560px">
                  <!-- Comment Row -->
                  <div class="d-flex flex-row comment-row mt-0">
                    <div class="p-2">
                      <img
                        src="assets/images/1.jpg"
                        alt="user"
                        width="50"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="comment-text w-100">
                      <h6 class="font-medium">James Anderson</h6>
                      <span class="mb-3 d-block"
                        >Lorem Ipsum is simply dummy text of the printing and type setting industry.
                      </span>
                      <div class="comment-footer d-md-flex align-items-center">
                        <span class="badge bg-primary rounded">Pending</span>
                        <span class="action-icons">
                          <a href="javascript:void(0)"
                            ><i data-feather="edit-3" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="check-circle" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="heart" class="feather-sm"></i
                          ></a>
                        </span>
                        <div class="text-muted ms-auto mt-2 mt-md-0">April 14, 2016</div>
                      </div>
                    </div>
                  </div>
                  <!-- Comment Row -->
                  <div class="d-flex flex-row comment-row">
                    <div class="p-2">
                      <img
                        src="assets/images/4.jpg"
                        alt="user"
                        width="50"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="comment-text active w-100">
                      <h6 class="font-medium">Michael Jorden</h6>
                      <span class="mb-3 d-block"
                        >Lorem Ipsum is simply dummy text of the printing and type setting industry.
                      </span>
                      <div class="comment-footer d-md-flex align-items-center">
                        <span class="badge bg-success rounded">Approved</span>
                        <span class="action-icons active">
                          <a href="javascript:void(0)"
                            ><i data-feather="edit-3" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="x-circle" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="heart" class="feather-sm text-danger"></i
                          ></a>
                        </span>
                        <div class="text-muted ms-auto mt-2 mt-md-0">April 14, 2016</div>
                      </div>
                    </div>
                  </div>
                  <!-- Comment Row -->
                  <div class="d-flex flex-row comment-row">
                    <div class="p-2">
                      <img
                        src="assets/images/5.jpg"
                        alt="user"
                        width="50"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="comment-text w-100">
                      <h6 class="font-medium">Johnathan Doeting</h6>
                      <span class="mb-3 d-block"
                        >Lorem Ipsum is simply dummy text of the printing and type setting industry.
                      </span>
                      <div class="comment-footer d-md-flex align-items-center">
                        <span class="badge rounded bg-danger">Rejected</span>
                        <span class="action-icons">
                          <a href="javascript:void(0)"
                            ><i data-feather="edit-3" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="check-circle" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="heart" class="feather-sm"></i
                          ></a>
                        </span>
                        <div class="text-muted ms-auto mt-2 mt-md-0">April 14, 2016</div>
                      </div>
                    </div>
                  </div>
                  <!-- Comment Row -->
                  <div class="d-flex flex-row comment-row">
                    <div class="p-2">
                      <img
                        src="assets/images/1.jpg"
                        alt="user"
                        width="50"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="comment-text w-100">
                      <h6 class="font-medium">James Anderson</h6>
                      <span class="mb-3 d-block"
                        >Lorem Ipsum is simply dummy text of the printing and type setting industry.
                      </span>
                      <div class="comment-footer d-md-flex align-items-center">
                        <span class="badge rounded bg-primary">Pending</span>
                        <span class="action-icons">
                          <a href="javascript:void(0)"
                            ><i data-feather="edit-3" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="check-circle" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="heart" class="feather-sm"></i
                          ></a>
                        </span>
                        <div class="text-muted ms-auto mt-2 mt-md-0">April 14, 2016</div>
                      </div>
                    </div>
                  </div>
                  <!-- Comment Row -->
                  <div class="d-flex flex-row comment-row">
                    <div class="p-2">
                      <img
                        src="assets/images/4.jpg"
                        alt="user"
                        width="50"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="comment-text active w-100">
                      <h6 class="font-medium">Michael Jorden</h6>
                      <span class="mb-3 d-block"
                        >Lorem Ipsum is simply dummy text of the printing and type setting industry.
                      </span>
                      <div class="comment-footer d-md-flex align-items-center">
                        <span class="badge bg-success rounded">Approved</span>
                        <span class="action-icons active">
                          <a href="javascript:void(0)"
                            ><i data-feather="edit-3" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="x-circle" class="feather-sm"></i
                          ></a>
                          <a href="javascript:void(0)"
                            ><i data-feather="heart" class="feather-sm text-danger"></i
                          ></a>
                        </span>
                        <div class="text-muted ms-auto mt-2 mt-md-0">April 14, 2016</div>
                      </div>
                    </div>
                  </div>
                  <!-- Comment Row -->
                </div>
              </div>
              <!-- ---------------------
                            end Recent Comments
                        ---------------- -->
            </div>
            <!-- column -->
            <div class="col-lg-6">
              <!-- ---------------------
                            start Recent Chats
                        ---------------- -->
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Recent Chats</h4>
                  <div class="chat-box scrollable position-relative" style="height: 475px">
                    <!--chat Row -->
                    <ul class="chat-list">
                      <!--chat Row -->
                      <li class="chat-item">
                        <div class="chat-img">
                          <img src="assets/images/1.jpg" alt="user" />
                        </div>
                        <div class="chat-content">
                          <h6 class="font-medium">James Anderson</h6>
                          <div class="box bg-light">
                            Lorem Ipsum is simply dummy text of the printing &amp; type setting
                            industry.
                          </div>
                        </div>
                        <div class="chat-time">10:56 am</div>
                      </li>
                      <!--chat Row -->
                      <li class="chat-item">
                        <div class="chat-img">
                          <img src="assets/images/2.jpg" alt="user" />
                        </div>
                        <div class="chat-content">
                          <h6 class="font-medium">Bianca Doe</h6>
                          <div class="box bg-light">It’s Great opportunity to work.</div>
                        </div>
                        <div class="chat-time">10:57 am</div>
                      </li>
                      <!--chat Row -->
                      <li class="odd chat-item">
                        <div class="chat-content">
                          <div class="box bg-light-inverse">I would love to join the team.</div>
                          <br />
                        </div>
                      </li>
                      <!--chat Row -->
                      <li class="odd chat-item">
                        <div class="chat-content">
                          <div class="box bg-light-inverse">Whats budget of the new project.</div>
                          <br />
                        </div>
                        <div class="chat-time">10:59 am</div>
                      </li>
                      <!--chat Row -->
                      <li class="chat-item">
                        <div class="chat-img">
                          <img src="assets/images/3.jpg" alt="user" />
                        </div>
                        <div class="chat-content">
                          <h6 class="font-medium">Angelina Rhodes</h6>
                          <div class="box bg-light">Well we have good budget for the project</div>
                        </div>
                        <div class="chat-time">11:00 am</div>
                      </li>
                      <!--chat Row -->
                      <!-- <div id="someDiv"></div> -->
                    </ul>
                  </div>
                </div>
                <div class="card-body border-top">
                  <div class="row">
                    <div class="col-9">
                      <div class="input-field mt-0 mb-0">
                        <input
                          type="text"
                          id="textarea1"
                          placeholder="Type and enter"
                          class="form-control border-0"
                        />
                      </div>
                    </div>
                    <div class="col-3 d-flex justify-content-end">
                      <a class="btn-circle btn-lg btn-cyan btn text-white" href="javascript:void(0)"
                        ><i data-feather="send" class="fill-white feather-sm"></i
                      ></a>
                    </div>
                  </div>
                </div>
              </div>
              <!-- ---------------------
                            end Recent Chats
                        ---------------- -->
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Recent comment and chats -->
          <!-- -------------------------------------------------------------- -->
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
<footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    <aside class="customizer">
      <a href="javascript:(0)" class="service-panel-toggle"
        ><i data-feather="settings" class="feather-sm fa fa-spin"></i
      ></a>
      <div class="customizer-body">
        <ul class="nav customizer-tab" role="tablist">
          <li class="nav-item">
            <a
              class="nav-link active"
              id="pills-home-tab"
              data-bs-toggle="pill"
              href="#pills-home"
              role="tab"
              aria-controls="pills-home"
              aria-selected="true"
              ><i class="ri-tools-fill fs-6"></i
            ></a>
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              id="pills-profile-tab"
              data-bs-toggle="pill"
              href="#chat"
              role="tab"
              aria-controls="chat"
              aria-selected="false"
              ><i class="ri-message-3-line fs-6"></i
            ></a>
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              id="pills-contact-tab"
              data-bs-toggle="pill"
              href="#pills-contact"
              role="tab"
              aria-controls="pills-contact"
              aria-selected="false"
              ><i class="ri-timer-line fs-6"></i
            ></a>
          </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
          <!-- Tab 1 -->
          <div
            class="tab-pane fade show active"
            id="pills-home"
            role="tabpanel"
            aria-labelledby="pills-home-tab"
          >
            <div class="p-3 border-bottom">
              <!-- Sidebar -->
              <h5 class="font-medium mb-2 mt-2">Layout Settings</h5>
              <div class="form-check mt-2">
                <input type="checkbox" class="form-check-input" name="theme-view" id="theme-view" />
                <label class="form-check-label" for="theme-view">Dark Theme</label>
              </div>
              <div class="form-check mt-2">
                <input
                  type="checkbox"
                  class="form-check-input sidebartoggler"
                  name="collapssidebar"
                  id="collapssidebar"
                />
                <label class="form-check-label" for="collapssidebar">Collapse Sidebar</label>
              </div>
              <div class="form-check mt-2">
                <input
                  type="checkbox"
                  class="form-check-input"
                  name="sidebar-position"
                  id="sidebar-position"
                />
                <label class="form-check-label" for="sidebar-position">Fixed Sidebar</label>
              </div>
              <div class="form-check mt-2">
                <input
                  type="checkbox"
                  class="form-check-input"
                  name="header-position"
                  id="header-position"
                />
                <label class="form-check-label" for="header-position">Fixed Header</label>
              </div>
              <div class="form-check mt-2">
                <input
                  type="checkbox"
                  class="form-check-input"
                  name="boxed-layout"
                  id="boxed-layout"
                />
                <label class="form-check-label" for="boxed-layout">Boxed Layout</label>
              </div>
            </div>
            <div class="p-3 border-bottom">
              <!-- Logo BG -->
              <h5 class="font-medium mb-2 mt-2">Logo Backgrounds</h5>
              <ul class="theme-color">
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin1"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin2"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin3"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin4"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin5"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-logobg="skin6"></a>
                </li>
              </ul>
              <!-- Logo BG -->
            </div>
            <div class="p-3 border-bottom">
              <!-- Navbar BG -->
              <h5 class="font-medium mb-2 mt-2">Navbar Backgrounds</h5>
              <ul class="theme-color">
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin1"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin2"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin3"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin4"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin5"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-navbarbg="skin6"></a>
                </li>
              </ul>
              <!-- Navbar BG -->
            </div>
            <div class="p-3 border-bottom">
              <!-- Logo BG -->
              <h5 class="font-medium mb-2 mt-2">Sidebar Backgrounds</h5>
              <ul class="theme-color">
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin1"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin2"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin3"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin4"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin5"></a>
                </li>
                <li class="theme-item">
                  <a href="#" class="theme-link" data-sidebarbg="skin6"></a>
                </li>
              </ul>
              <!-- Logo BG -->
            </div>
          </div>
          <!-- End Tab 1 -->
          <!-- Tab 2 -->
          <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="pills-profile-tab">
            <ul class="mailbox list-style-none mt-3">
              <li>
                <div class="message-center chat-scroll">
                  <a href="#" class="message-item" id="chat_user_1" data-user-id="1">
                    <span class="user-img">
                      <img
                        src="assets/images/1.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status online pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:30 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_2" data-user-id="2">
                    <span class="user-img">
                      <img
                        src="assets/images/2.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status busy pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Sonu Nigam</h5>
                      <span class="mail-desc">I've sung a song! See you at</span>
                      <span class="time">9:10 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_3" data-user-id="3">
                    <span class="user-img">
                      <img
                        src="assets/images/3.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status away pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Arijit Sinh</h5>
                      <span class="mail-desc">I am a singer!</span>
                      <span class="time">9:08 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_4" data-user-id="4">
                    <span class="user-img">
                      <img
                        src="assets/images/4.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status offline pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Nirav Joshi</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:02 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_5" data-user-id="5">
                    <span class="user-img">
                      <img
                        src="assets/images/5.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status offline pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Sunil Joshi</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:02 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_6" data-user-id="6">
                    <span class="user-img">
                      <img
                        src="assets/images/6.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status offline pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Akshay Kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:02 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_7" data-user-id="7">
                    <span class="user-img">
                      <img
                        src="assets/images/7.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status offline pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Pavan kumar</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:02 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                  <!-- Message -->
                  <a href="#" class="message-item" id="chat_user_8" data-user-id="8">
                    <span class="user-img">
                      <img
                        src="assets/images/8.jpg"
                        alt="user"
                        class="rounded-circle"
                      />
                      <span class="profile-status offline pull-right"></span>
                    </span>
                    <div class="mail-contnet">
                      <h5 class="message-title">Varun Dhavan</h5>
                      <span class="mail-desc">Just see the my admin!</span>
                      <span class="time">9:02 AM</span>
                    </div>
                  </a>
                  <!-- Message -->
                </div>
              </li>
            </ul>
          </div>
          <!-- End Tab 2 -->
          <!-- Tab 3 -->
          <div
            class="tab-pane fade p-3"
            id="pills-contact"
            role="tabpanel"
            aria-labelledby="pills-contact-tab"
          >
            <h6 class="mt-3 mb-3">Activity Timeline</h6>
            <div class="steamline">
              <div class="sl-item">
                <div class="sl-left bg-light-success text-success">
                  <i data-feather="user" class="feather-sm fill-white"></i>
                </div>
                <div class="sl-right">
                  <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                  <div class="desc">you can write anything</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left bg-light-info text-info">
                  <i data-feather="camera" class="feather-sm fill-white"></i>
                </div>
                <div class="sl-right">
                  <div class="font-medium">Send documents to Clark</div>
                  <div class="desc">Lorem Ipsum is simply</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left">
                  <img class="rounded-circle" alt="user" src="assets/images/2.jpg" />
                </div>
                <div class="sl-right">
                  <div class="font-medium">
                    Go to the Doctor <span class="sl-date">5 minutes ago</span>
                  </div>
                  <div class="desc">Contrary to popular belief</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left">
                  <img class="rounded-circle" alt="user" src="assets/images/1.jpg" />
                </div>
                <div class="sl-right">
                  <div>
                    <a href="#">Stephen</a>
                    <span class="sl-date">5 minutes ago</span>
                  </div>
                  <div class="desc">Approve meeting with tiger</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left bg-light-primary text-primary">
                  <i data-feather="user" class="feather-sm fill-white"></i>
                </div>
                <div class="sl-right">
                  <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                  <div class="desc">you can write anything</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left bg-light-info text-info">
                  <i data-feather="send" class="feather-sm fill-white"></i>
                </div>
                <div class="sl-right">
                  <div class="font-medium">Send documents to Clark</div>
                  <div class="desc">Lorem Ipsum is simply</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left">
                  <img class="rounded-circle" alt="user" src="assets/images/4.jpg" />
                </div>
                <div class="sl-right">
                  <div class="font-medium">
                    Go to the Doctor <span class="sl-date">5 minutes ago</span>
                  </div>
                  <div class="desc">Contrary to popular belief</div>
                </div>
              </div>
              <div class="sl-item">
                <div class="sl-left">
                  <img class="rounded-circle" alt="user" src="assets/images/6.jpg" />
                </div>
                <div class="sl-right">
                  <div>
                    <a href="#">Stephen</a>
                    <span class="sl-date">5 minutes ago</span>
                  </div>
                  <div class="desc">Approve meeting with tiger</div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Tab 3 -->
        </div>
      </div>
    </aside>
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <script src="assets/js/apexcharts.min.js"></script>
    <script src="assets/js/pages/dashboards/dashboard1.js"></script>
  </body>
</html>
