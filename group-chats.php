
<?php
  include("session.php");
  include("encryption/index.php");
  include("files/getFile.php");

  $username = "";
  $groupID = "";

  if($_SERVER["REQUEST_METHOD"]=="POST"){
    $username = mysqli_real_escape_string($db,$_POST["user_id"]);
    $groupID = mysqli_real_escape_string($db,$_POST["group_id"]);
  }
  else if($_SERVER["REQUEST_METHOD"]=="GET"){
    $username = mysqli_real_escape_string($db,$_GET["user_id"]);
    $groupID = mysqli_real_escape_string($db,$_GET["group_id"]);
  }

  $groupProfile = array();

  $chatMessages = array();

  if($groupID!=null){

    $getGroupProfileSQL = "SELECT * FROM GroupsData WHERE group_db_id='$groupID'";
    $getMessagesSql = "SELECT * FROM messages WHERE 
                      (group_database_id='$groupID')
                      order by sent_timestamp;
                      ";

    $userMessagesResult = mysqli_query($db,$getMessagesSql);
    $groupProfileResult = mysqli_query($db,$getGroupProfileSQL);
    
    $groupProfile = mysqli_fetch_assoc($groupProfileResult);

    while($row=mysqli_fetch_array($userMessagesResult)){
        echo "<script>console.log('".$row["message_type"]."') </script>";
        array_push($chatMessages,$row);
    }

  }
  else{
    header("location: app-contacts.php");
  }

  function getdecryptedData($fileUrl){
    $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/decryptFile";

    //The data you want to send via POST
    $fields = [
      'url' => $fileUrl // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
    ];

    //url-ify the data for the POST 
    $fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data   
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

    //execute post
    $result = curl_exec($ch);

    echo "<script>console.log('$result')</script>";

    return $result;
  }

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- This page css -->
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php 
        include("ui/sidebar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->

      <script>

        var messages = [];
        var sortedMessages = [];
        var userName = null;

        function setUserName(name){
          userName = name;
        }

      
        function clearMessages(){
          messages = [];
          sortedMessages = [];
        }

        function addNewMessage(messageType,sender_database_id,receiver_database_id,messageOrFileData,mediaUrl,timestamp){
          messages.push({messageType,messageOrFileData,sender_database_id,receiver_database_id,mediaUrl,timestamp});
          sortedMessages.push({messageType,messageOrFileData,sender_database_id,receiver_database_id,mediaUrl,timestamp});
          addToUI();
        }

        function search(){
          var val = document.getElementById("searchInputVal").value;
          console.log("Searching val = ",val);
          sortedMessages = []; 
          for(var i=0;i<messages.length;i++){
            if((messages[i]["messageType"]===val) || (messages[i]["messageOrFileData"] === val) || (messages[i]["messageType"]==="text" && messages[i]["messageOrFileData"].toString().toLowerCase().includes(val.toLowerCase()))){
              sortedMessages.push(messages[i]);
            }   
          }
          addToUI();
        }

        function getMediaHtml(messageType,mediaUrl,mediaData,type){
          //console.log("Url = ",mediaUrl);
          if(!mediaData){
            return "not url";
          }
          switch(messageType){
            case "image" : return `<img height="100" width="100" src="data:image/jpg;base64, ${mediaData}" />`;break;
            case "audio" : return `<audio controls="controls" autobuffer="autobuffer" autoplay="autoplay">
                                    <source src="data:audio/wav;base64,${mediaData}" />
                                  </audio>`;
                                  break;
            case "video" : return `<video width="320" height="240" controls>
                                    <source src="data:video/mp4;base64,${mediaData}" type="video/mp4">
                                      Your browser does not support the video tag.
                                  </video>`;
            case "file" : const urlData = new URL(mediaUrl);
                          const splitData = urlData.pathname.split(".");
                          const fileExtension = splitData[splitData.length-1];
                          const fileName = `${Date.now()}.${fileExtension}`;
                          return `<a href="data:application/octet-stream;base64,${mediaData}" download="${fileName}" style="color:${type=="odd"?"white":"black"}" >Download File ${fileName}</a>`
          }

          return "null";
        }

        function addToUI(){

          var html = "";

          console.log("sorted messages = ",sortedMessages);

          for(var i=0;i<sortedMessages.length;i++){
            var message = sortedMessages[i];

            console.log("Sender database id = ",message["sender_database_id"]," username = ",userName);

            if(message["sender_database_id"]===userName){
              html += `<li class="chat-item">
                        <div class="chat-content">
                          <p> ${message["sender_database_id"]} </p>
                          <div class="box bg-light">
                            ${message["messageType"]=="text"?message["messageOrFileData"]: getMediaHtml( message["messageType"],message["mediaUrl"], message["messageOrFileData"],"even") }
                          </div>
                        </div>
                        <div class="chat-time">${new Date(message['timestamp']).toLocaleString()}</div>
                      </li>
                      `;
            }
            else{
              
              html += `<li class="odd chat-item">
                            <div class="chat-content">
                              <div class="box bg-light-inverse">${message["messageType"]=="text"?message["messageOrFileData"]: getMediaHtml(message["messageType"],message["mediaUrl"],message["messageOrFileData"],"odd") }</div>
                              <br />
                            </div>
                            <div class="chat-time">${new Date(message['timestamp']).toLocaleString()}</div>
                          </li>
                        `;
            }
                      
          }

        
          document.getElementById("messages-list").innerHTML = html;

        }

      </script>

      <?php

        echo "<script>setUserName('$username');</script>";

      ?>
      

      <div class="page-wrapper">
        <div class="chat-application">
        <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo ($groupProfile==null?$groupID:$groupProfile["group_name"]); ?></h4>
                  <div class="chat-box scrollable position-relative" style="height: 475px">
                    <!--chat Row -->
                    <ul class="chat-list" id="messages-list">
                    <?php

                    echo "<script>clearMessages()</script>";

                    foreach($chatMessages as $message){

                      $messageType = $message["message_type"]; 

                      $mediaUrl = NULL;

                      if($messageType =="image"){
                        $mediaUrl = $message["image_url"];
                      }
                      else if($messageType=="video"){
                        $mediaUrl = $message["video_url"];
                      }
                      else if($messageType=="audio"){
                        $mediaUrl = $message["audio_url"];
                      }
                      else if($messageType=="file"){
                        $mediaUrl = $message["file_url"];
                      }

                      echo "<script>console.log('file url = ".decryptMessage($mediaUrl)." ')</script>";

                      $mediaUrl = $mediaUrl == null ? null : decryptMessage($mediaUrl);

                      if($mediaUrl==NULL){
                        if($messageType!="contact"){
                          $mediaDataOrText = decryptMessage($message["text_message"]);
                        }
                        else{
                          $mediaDataOrText = decryptMessage($message["contact_json"]);
                          $messageType = "text";
                        }
                      }
                      else{
                        $mediaDataOrText = getdecryptedData($mediaUrl);
                      }

                      try{
                        //echo "<script>console.log('its working message = $mediaDataOrText')</script>";
                        echo "<script>addNewMessage('$messageType','".$message['sender_database_id']."','".$message['receiver_database_id']."','".$mediaDataOrText."', '".$mediaUrl."' ,".intval($message['sent_timestamp']).")</script>";
                      }
                      catch(Exception $e){
                        echo "<script>console.log('the error is ".$e->getMessage()."');";
                      }
                    }

                    echo "<script>addToUI();</script>";

                  ?>
                     
                    </ul>
                  </div>
                </div>
              </div>

            <div class="card card-body">
              <div class="row">
                <div class="col-md-12 col-xl-2">
                    <div class="row">
                      <div class="col-md-3 col-xl-2">
                        <input
                          type="text"
                          id="searchInputVal"
                          name="search_val"
                          class="form-control"
                          placeholder="Search Messages..."
                        />
                      </div>
                      <div class="col-md-2 col-xl-2">
                        <button class="btn btn-info" onclick="search()">
                          <i data-feather="search" class="feather-sm fill-white me-1"> </i>
                            Search
                        </button>
                      </div>
                     
                    </div>
                </div>
              </div>
            </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Left Part  -->
          <!-- -------------------------------------------------------------- -->
          
          <!-- -------------------------------------------------------------- -->
          <!-- End Left Part  -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Right Part  Mail Compose -->
          <!-- -------------------------------------------------------------- -->
        
        </div>

        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
  
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/extra-libs/taskboard/js/jquery.ui.touch-punch-improved.js"></script>
    <script src="assets/extra-libs/taskboard/js/jquery-ui.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="assets/libs/popper.js/popper.min.js"></script> -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/pages/chat/chat.js"></script>
  </body>
</html>
