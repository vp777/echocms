 <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
          <!-- Sidebar navigation-->
          <nav class="sidebar-nav">
            <ul id="sidebarnav">
              <!-- User Profile-->
             

              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="app-contacts.php"
                  aria-expanded="false"
                  ><i data-feather="phone" class="feather-icon"></i
                  ><span class="hide-menu">Users</span></a
                >
              </li>

              
              <?php

                if($_SESSION["user_role"]=="admin"){
                echo '
                      <li class="sidebar-item">
                        <a
                            class="sidebar-link waves-effect waves-dark sidebar-link"
                            href="app-controls.php"
                            aria-expanded="false"
                          ><i data-feather="settings" class="feather-icon"></i>
                          <span class="hide-menu">App Controls</span>
                        </a>
                      </li>
                      <li class="sidebar-item">
                      <a
                          class="sidebar-link waves-effect waves-dark sidebar-link"
                          href="admin_users.php"
                          aria-expanded="false"
                        >
                        <i data-feather="shield" class="feather-icon"></i>
                        <span class="hide-menu">Admin Users</span>
                      </a>
                      </li>';
                }

              ?>

              <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="logout.php"
                  aria-expanded="false"
                  ><i data-feather="log-out" class="feather-icon"></i
                  ><span class="hide-menu">Logout</span></a
                >
              </li>
              

              
            </ul>
          </nav>
          <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
      </aside>