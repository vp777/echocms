<?php

include('session.php');
error_reporting(0);

$username = "";

$searchValue = null;

if($_SERVER["REQUEST_METHOD"]=="POST"){
  $username = mysqli_real_escape_string($db,$_POST["user_id"]);
  if(isset($_POST["search_val"])){
    $searchValue = mysqli_real_escape_string($db,$_POST["search_val"]);
  }
}
else{
  $username = mysqli_real_escape_string($db,$_GET["user_id"]);
  if(isset($_GET["search_val"])){
    $searchValue = mysqli_real_escape_string($db,$_GET["search_val"]);
  }
}


if($searchValue==null){
    $groupSQL = "SELECT * FROM GroupsData WHERE participants like '%$username%'";
}
else{
    $groupSQL = "SELECT * FROM GroupsData WHERE (participants like '%$username%') and (group_name like '%$searchValue%') ";
}

$groupResult = mysqli_query($db,$groupSQL);


?>



<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- This page plugin CSS -->
    <link href="assets/css/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
include("ui/preloader.php");
?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include('ui/sidebar.php')
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">User Groups <?php echo $username; ?></h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                  </ol>
                </nav>
              </div>
            </div>
         
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->
          <div class="widget-content searchable-container list">
            <!-- ---------------------
                        start Contact
                    ---------------- -->
            <div class="card card-body">
              <div class="row">
                <div class="col-md-6 col-xl-2">
                  <form action="" method="POST">
                    <div class="row">
                      <div class="col-md-6 col-xl-2">
                        <input
                          type="text"
                          name="search_val"
                          class="form-control"
                          placeholder="Search..."
                        />
                      </div>
                      <div class="col-md-4 col-xl-2">
                        <button class="btn btn-info">
                          <i data-feather="search" class="feather-sm fill-white me-1"> </i>
                            Search
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- ---------------------
                        end Contact
                    ---------------- -->
            <!-- Modal -->
            
            <div class="card card-body">
              <div class="table-responsive">
                <table class="table search-table v-middle text-nowrap">
                  <thead class="header-item">
                    <th>Group Name</th>
                    <th>Create date</th>
                    <th>Created by</th>
                    <th>Admin</th>
                    <th>Action</th>
                  </thead>
                  <tbody>

                    <script>
                        function getDateString(id,timestamp){
                            var date = new Date(timestamp);
                            document.getElementById(id).innerHTML = date.toLocaleDateString();
                        }
                    </script>


                    <?php 
                    
                      while($row = mysqli_fetch_array($groupResult)){
                        echo "<tr>";
                        echo '<td>
                                <div class="d-flex align-items-center">
                                '.
                                ($row["group_icon_image"]=="null"?
                                ""
                                :
                                "<img
                                  src='".$row["group_icon_image"]."'
                                  alt='avatar'
                                  class='rounded-circle'
                                  width='40'
                                  height='40'
                                />").
                                '
                                <div class="ms-2">
                                  <div class="user-meta-info">
                                    <h5 class="user-name mb-0" data-name="'.$row["group_name"].'">'.$row["group_name"].'</h5>
                                    <span class="user-work text-muted">'.$row["group_desc"].'</span>
                                  </div>
                                </div>
                              </div>
                            </td>';
                        $groupDbId = $row["group_db_id"];
                        $create_date = $row["create_date"];
                        echo "<td id='".$row["group_db_id"]."'><td>";
                        echo "<td>".$row["created_by"]."</td>";
                        echo "<td>".$row["admin"]."</td>";
                        echo '<td>
                                <div class="action-btn">
                                  <form action="group-chats.php" method="POST">
                                    <input name="user_id" value="'.$username.'" type="hidden" />
                                    <input name="group_id" value="'.$row['group_db_id'].'" type="hidden" />
                                    <button style="border:none;backgroundColor:white;color:white" class="text-info edit">
                                      <i data-feather="eye" class="feather-sm fill-white"></i>
                                    </button>
                                  </form>
                                </div>
                              </td>';
                        echo "</tr>";
                        echo "<script>getDateString('".$groupDbId."',".$create_date.")</script>";
                        
                      }
                    ?>
        
               

                 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- -------------------------------------------------------------- -->
          <!-- End PAge Content -->
          <!-- -------------------------------------------------------------- -->
        </div>
        
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
          <footer class="footer text-center">
            All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <script src="assets/js/pages/contact/contact.js"></script>
  </body>
</html>