<?php

include("session.php");

$username = "";
$otherUserId = null;
$searchValue = null;

$start = 0;

if($_SERVER["REQUEST_METHOD"]=="POST"){
  $username = mysqli_real_escape_string($db,$_POST["user_id"]);
  if(isset($_POST["other_user"])){
    $otherUserId =  mysqli_real_escape_string($db,$_POST["other_user"]);
  }
  if(isset($_POST["search_val"])){
    $searchValue = mysqli_real_escape_string($db,$_POST["search_val"]);
  }
  if(isset($_POST["start"])){
    $start = $_POST["start"];
  }
}
else if($_SERVER["REQUEST_METHOD"]=="GET"){
  $username = mysqli_real_escape_string($db,$_GET["user_id"]);
  if(isset($_GET["other_user"])){
    $otherUserId = mysqli_real_escape_string($db,$_GET["other_user"]);
  }
  if(isset($_GET["search_val"])){
    $searchValue = mysqli_real_escape_string($db,$_GET["search_val"]);
  }
  if(isset($_GET["start"])){
    $start = $_GET["start"];
  }
}


$sql = "";

if($searchValue==null){

/***
  $sql = "SELECT distinct
          sender_database_id,receiver_database_id,sent_timestamp
          FROM messages 
          where 
          sender_database_id='$username' or receiver_database_id='$username'
          order by sent_timestamp desc limit 10 offset $start";
***/   

$sql = "SELECT distinct
          sender_database_id,receiver_database_id,sent_timestamp
          FROM messages 
          where 
          sender_database_id='$username' or receiver_database_id='$username'
          order by sent_timestamp desc";
          
          
          
}
else{
  $sql = "SELECT distinct sender_database_id,receiver_database_id,sent_timestamp FROM messages 
          where
          (receiver_database_id like '%$searchValue%' or sender_database_id like '%$searchValue%')
          and 
          (sender_database_id='$username' or receiver_database_id='$username')
          order by sent_timestamp desc
          limit 10
          OFFSET $start
          ";
}

echo "<script>console.log('$sql')</script>";

$messagesResult = mysqli_query($db,$sql);

$count = mysqli_num_rows($messagesResult);  

$user_ids = array();

$profiles = array();

$groupsData = array();

while($row = mysqli_fetch_array($messagesResult)){
  if(!in_array($row["sender_database_id"],$user_ids)){
    if($row["sender_database_id"]!=$username){
      array_push($user_ids,$row["sender_database_id"]);
    }
  }
  if(!in_array($row["receiver_database_id"],$user_ids)){
    if($row["receiver_database_id"]!=$username){
      array_push($user_ids,$row["receiver_database_id"]);
    }
  }
}



echo "<script>
    console.log('".implode(",", $user_ids)."');
  </script>";


foreach($user_ids as $user_id){
  $lastMessageSql = "SELECT * FROM messages WHERE receiver_database_id='$user_id' or sender_database_id='$user_id' order by sent_timestamp desc limit 1";
  $profileSql = "SELECT * FROM USERS WHERE user_id='$user_id'";
  $lastMessage = mysqli_query($db,$lastMessageSql);
  $profileResult = mysqli_query($db,$profileSql);
  
  $value = array();
  $value["profile"] = mysqli_num_rows($profileResult) == 0  ? NULL  :  mysqli_fetch_assoc($profileResult);
  $value["message"] = mysqli_num_rows($lastMessage) ==0 ? NULL  : mysqli_fetch_assoc($lastMessage);
  $value["user_id"] = $user_id;

  array_push($profiles,$value);
}



?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- This Page CSS -->
    <link rel="stylesheet" type="text/css" href="assets/extra-libs/prism/prism.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php 
        include("ui/sidebar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">Chat List</h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Chats</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Chat List</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->

          <div class="card card-body">
              <div class="row">
                <div class="col-lg-12">
                  <form action="" method="POST">
                    <input type="hidden" name="user_id" value="<?php echo $username ?>" />
                    <div class="row">
                      <div class="col-md-6 col-xl-2">
                        <input
                          type="text"
                          name="search_val"
                          class="form-control"
                          placeholder="Search Users..."
                        />
                      </div>
                      <div class="col-md-2 col-xl-2">
                        <button class="btn btn-info">
                          <i data-feather="search" class="feather-sm fill-white me-1"> </i>
                            Search
                        </button>
                      </div>
                    </form>
                      <div class="col-md-4 col-xl-2">
                        <form action="user-groups.php" method="POST">
                          <input type="hidden" name="user_id" value="<?php echo $username; ?>" />
                          <button class="btn btn-info" type="submit">
                            <i data-feather="users" class="feather-sm fill-white me-1"> </i>
                              Open Group
                          </button>
                        </form> 
                      </div>
                    </div>
                </div>
              </div>
            </div>

          <div class="card">
            <div class="card-body">
              <h4 class="card-title"><?php echo $username ?></h4>
            </div>

            
            <div class="table-responsive">
              <table class="table customize-table mb-0 v-middle">
                <thead class="table-light">
                  <tr>
                    <th class="border-bottom border-top">Phone</th>
                    <th class="border-bottom border-top">User</th>
                    <th class="border-bottom border-top">Last Message Time</th>
                    <th class="border-bottom border-top">View</th>
                  </tr>
                </thead>
                <tbody>

                <?php

                    $i = 0;
                    foreach($profiles as $profile){
                      if($profile["user_id"]!=null && $profile["user_id"]!="null"){
                        $i += 1;
                        $profileUserId = $profile["user_id"];
                        $url = 'app-chats.php?user_id='.$username.'&other_user='.$profileUserId;
                        echo '
                        <tr>
                            <td>'.($profile["user_id"]).'</td>
                            <td>
                                <div class="d-flex align-items-center">
                                '.
                                (
                                $profile["profile"]==NULL?"":
                                ($profile["profile"]["profile_picture"]==NULL?"":
                                "
                                <img 
                                  height='40'
                                  width='30'
                                  src='".$profile["profile"]["profile_picture"]."'
                                  alt='user'
                                  class='rounded-circle'
                                />
                                "
                                )).'
                                  <span class="ms-3 fw-normal">'.($profile["profile"]==NULL?"":$profile["profile"]["name"]).'</span>
                                </div>
                            </td>
                            <td>'.date('d-m-Y h:i a',(intval($profile["message"]["sent_timestamp"])/1000)).'</td>
                            <td>
                                <a href="'.$url.'">View</a>
                            </td>
                        </tr>

                        ';
                    }
                  }

                ?>
                 
                  
                  
                </tbody>
              </table>

            </div>
          </div>


          
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <?php

              $start = intval($start);
              if($start>0){
                  echo '<li class="page-item">
                          <form action="" method="POST">
                            <input type="hidden" name="start" value="'.($start-10).'" />
                              <button><span aria-hidden="true">&laquo;</span>Prev</button>
                          </form>
                        </li>';
              }
              ?>
            <li>

            <?php

            if(mysqli_num_rows($messagesResult)==10){

                echo '<form action="" method="POST">
                        <input type="hidden" name="start" value="'.($start+10).'" />
                        <button><span aria-hidden="true">&raquo;</span>Next</button>
                      </form>';
            }
            ?>
            </li>
        </ul>
      </nav>
          
          <!-- -------------------------------------------------------------- -->
          <!-- End PAge Content -->
          <!-- -------------------------------------------------------------- -->
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
<footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <script src="assets/extra-libs/prism/prism.js"></script>
  </body>
</html>