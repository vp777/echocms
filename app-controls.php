<?php

error_reporting(E_ERROR | E_PARSE); 




function updateData($fields){
    $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/writeControlsData";

    //The data you want to send via POST
    //$fields = [
    //  'url' => $fileUrl // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
    //];

    //url-ify the data for the POST 
    $fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data   
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

    //execute post
    $result = curl_exec($ch);

    echo "<script>console.log('$result')</script>";

    return $result;
  }

  function getData(){
    $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/getControlData";

    //The data you want to send via POST
    //$fields = [
    //  'url' => $fileUrl // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
    //];

    //url-ify the data for the POST 
    $fields_string = http_build_query([]);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data   
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

    //execute post
    $result = curl_exec($ch);

    echo "<script>console.log('$result')</script>";

    return $result;
  }

?>

?>

<?php

  include("session.php");
  include("encryption/index.php");
  include("files/getFile.php");

  if($_SESSION["user_role"]!="admin"){
    die("Access denied");
  }
  

  $shouldUpdate = false;

  $failureMessage = "";
  $successMessage = "";

  $audio_call = null;
  $video_call = null;
  $group_chat = null;
  $audio_call_limit = null;
  $video_call_limit = null;


  if($_SERVER["REQUEST_METHOD"]=="POST"){

    if(isset($_POST["updateData"])){

      $shouldUpdate = mysqli_real_escape_string($db,$_POST["updateData"])=="true";

      if($shouldUpdate){
          $audio_call = $_POST["audio_call"]?"active":"inactive";
          $video_call = $_POST["video_call"]?"active":"inactive";
          $group_chat = $_POST["group_chat"]?"active":"inactive";
          $audio_call_limit = mysqli_real_escape_string($db,$_POST["audio_call_limit"]);
          $video_call_limit = mysqli_real_escape_string($db,$_POST["video_call_limit"]);
      }
    
    }
  }
  else if($_SERVER["REQUEST_METHOD"]=="GET"){

    if(isset($_GET["updateData"])){

        $shouldUpdate = mysqli_real_escape_string($db,$_GET["updateData"])=="true";
  
        if($shouldUpdate){
            $audio_call = $_GET["audio_call"]?"active":"inactive";
            $video_call = $_GET["video_call"]?"active":"inactive";
            $group_chat = $_GET["group_chat"]?"active":"inactive";
            $audio_call_limit = mysqli_real_escape_string($db,$_GET["audio_call_limit"]);
            $video_call_limit = mysqli_real_escape_string($db,$_GET["video_call_limit"]);
        }
      
    }
  }


  if($shouldUpdate){
    $fields = [
        "audio_call" => $audio_call,
        "video_call" => $video_call,
        "group_chat" => $group_chat,
        "audio_call_limit" => $audio_call_limit,
        "video_call_limit" => $video_call_limit
    ];

    $result = updateData($fields);

    $json_result = json_decode($result);

    if($json_result->status==="failure"){
        $failureMessage = "failed to update controls ".$json_result['message'];
    }
    else{
        $successMessage = "Controls updated succesfully";
    }
  }


  $controlsDataResult = getData();

  echo "<script>console.log('data = ".$controlsDataResult." ')</script>";

  $controls_result = json_decode($controlsDataResult);

  if($controls_result->status=="failure"){
      $failureMessage = $controls_result["message"];
  }
  else{
      $audio_call = $controls_result->data->audio_call;
      $video_call = $controls_result->data->video_call;
      $group_chat = $controls_result->data->group_chat;
      $audio_call_limit = $controls_result->data->audio_call_limit;
      $video_call_limit = $controls_result->data->video_call_limit;
  }




?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- This Page CSS -->
    <link rel="stylesheet" type="text/css" href="assets/extra-libs/prism/prism.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php 
        include("ui/sidebar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div style="padding:14px">
            <p style="color:green"><?php echo $successMessage ?> </p>
            <p style="color:red"><?php echo $failureMessage ?> </p>
        </div>

        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">App Controls</h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Controls</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->

         

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">App Controls</h4>
                </div>

                <style>
                    .controls{
                        padding: 30px;
                    }
                </style>

                <form action="app-controls.php" method="POST">

                <div class="row py-2 controls">
                    <div class="col-md-4">
                        <label>Video Call</label>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input success" name="video_call" <?php echo ($video_call=="active"?"checked":"") ?> type="checkbox" id="success-check" >
                            <label class="form-check-label" for="success-check">Enabled</label>
                        </div>
                    </div>
                </div>
                        
                <div class="row py-2 controls">
                    <div class="col-md-4">
                        <label>Audio Call</label>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input success" name="audio_call" <?php echo ($audio_call=="active"?"checked":"") ?>  type="checkbox" id="success-check">
                            <label class="form-check-label" for="success-check">Enabled</label>
                        </div>
                    </div>
                </div>

                <div class="row py-2 controls">
                    <div class="col-md-4">
                        <label>Group Chatting</label>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input success" name="group_chat" <?php echo ($group_chat=="active"?"checked":"") ?> type="checkbox" id="success-check" >
                            <label class="form-check-label" for="success-check">Enabled</label>
                        </div>
                    </div>
                </div>

                <div class="row py-2 controls">
                    <div class="col-md-4">
                        <label>Audio Call Limit (Mins)</label>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="audio_call_limit" value="<?php echo $audio_call_limit ?>" placeholder="Enter Value in mins" name="audio_call_limit">
                        </div>
                    </div>
                </div>

                <div class="row py-2 controls">
                    <div class="col-md-4">
                        <label>Video Call Limit (Mins)</label>
                    </div>
                    
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="number" class="form-control" id="video_call_limit" value="<?php echo $video_call_limit ?>" placeholder="Enter value in mins" name="video_call_limit" >
                        </div>
                    </div>
                </div>

                <input type="hidden" name="updateData" value="true" />
                <div style="padding:15px">
                    <button type="submit" class="btn btn-info rounded-pill px-4 waves-effect waves-light">
                        Update
                    </button>
                </div>
                </form>



            </div>
            
            
          <!-- -------------------------------------------------------------- -->
          <!-- End PAge Content -->
          <!-- -------------------------------------------------------------- -->
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
<footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <script src="assets/extra-libs/prism/prism.js"></script>
  </body>
</html>