<?php

include("session.php");
include("encryption/index.php");

$username = "";
$otherUserId = null;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $username = mysqli_real_escape_string($db,$_POST["user_id"]);
    if(isset($_POST["other_user"])){
      $otherUserId =  mysqli_real_escape_string($db,$_POST["other_user"]);
    }
}
else if($_SERVER["REQUEST_METHOD"]=="GET"){
    $username = mysqli_real_escape_string($db,$_GET["user_id"]);
    if(isset($_GET["other_user"])){
      $otherUserId = mysqli_real_escape_string($db,$_GET["other_user"]);
    }
}

$callsData = array();

if($otherUserId == NULL){
    header("location: app-contacts.php");
}
else{

    $sql = "SELECT * FROM calls 
            where 
            (sender_uid='$username' or sender_uid='$otherUserId') 
            and 
            ( receiver_uid='$otherUserId' or receiver_uid='$username') 
            order by created_on desc limit 10;";

    $callsResult = mysqli_query($db,$sql);

    while($row=mysqli_fetch_array($callsResult)){
        array_push($callsData,$row);
    }

}


?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- This Page CSS -->
    <link rel="stylesheet" type="text/css" href="assets/extra-libs/prism/prism.css" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php 
        include("ui/sidebar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">Calls <?php echo $username; ?> </h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Chat List</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->

         

          <div class="card">
            <div class="card-body">
              <h4 class="card-title"><?php echo $username ?> - <?php echo $otherUserId ?></h4>
            </div>

            

            
            <div class="table-responsive">
              <table class="table customize-table mb-0 v-middle">
                <thead class="table-light">
                  <tr>
                    <th class="border-bottom border-top">Caller</th>
                    <th class="border-bottom border-top">Receiver</th>
                    <th class="border-bottom border-top">Duration</th>
                    <th class="border-bottom border-top">Time</th>
                    <th class="border-bottom border-top">Download</th>
                  </tr>
                </thead>
                <tbody>

                <script>
                    function printTime(id,timestamp){
                        const date = new Date(parseInt(timestamp));

                        console.log("Timestamp = ",timestamp, "date = ",date);

                        document.getElementById(id).innerHTML = date.toLocaleString();
                    }
                </script>

                <?php 


                  function getBase64Data($fileKey){
                    $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/getAudioFileData";

                    //The data you want to send via POST
                    $fields = [
                      'fileKey' => $fileKey // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
                    ];

                    //url-ify the data for the POST 
                    $fields_string = http_build_query($fields);

                    //open connection
                    $ch = curl_init();

                    //set the url, number of POST vars, POST data   
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                    //So that curl_exec returns the contents of the cURL; rather than echoing it
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

                    //execute post
                    $result = curl_exec($ch);

                    echo "<script>console.log('$result')</script>";

                    return $result;
                  }

                ?>

                <?php

                    $i = 0;
                    foreach($callsData as $call){

                        $i += 1;

                        $fileJSON = $call["file_list"] == null ? null : stripslashes(html_entity_decode(decryptMessage($call["file_list"])));
                        
                        echo "<script>console.log('$fileJSON');</script>";

                        $fileName = null;

                        if($fileJSON!=null){

                          echo "<script>console.log('entered into if loop')</script>";

                          $fileJSON = json_decode($fileJSON,true);

                          foreach($fileJSON["fileList"] as $fileData){
                            if(strpos($fileData["fileName"],".mp4")){
                              $fileName = $fileData["fileName"];
                              echo "<script>console.log('File name = ".$fileData["fileName"]." ' );</script>";
                            }
                          }

                        }

                        $downloadHTML = "";

                        if($fileName==null){
                          $downloadHTML = "No File";
                        }
                        else{
                          $downloadHTML =  '
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal'.$i.'">
                            Open
                          </button>

                          <div class="modal fade" id="modal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="modal'.$i.'" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Call</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <iframe src="calls_player.php?fileName='.$fileName.'" height="500px" width="500px" ></iframe>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                          ';
                        }
                        
                        //<a target="_blank" href="calls_player.php?fileName=$fileName">Open</a>';

                        $i += 1;
                        echo '
                        <tr>
                            <td>'.($call["sender_uid"]).'</td>
                            <td>'.($call["receiver_uid"]).'</td>
                            <td>'.($call["duration"]).' seconds</td>
                            <td id='.$call["call_id"].'><script>printTime("'.$call["call_id"].'","'.($call["created_on"] ).'");</script></td>
                            <td>
                              '.$downloadHTML.'
                            </td>
                        </tr>

                        ';
                    }

                ?>
                 
                  
                  
                </tbody>
              </table>
            </div>
          </div>


          
          <!-- -------------------------------------------------------------- -->
          <!-- End PAge Content -->
          <!-- -------------------------------------------------------------- -->
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
<footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
      
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <script src="assets/extra-libs/prism/prism.js"></script>
  </body>
</html>