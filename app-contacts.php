<?php

include('session.php');


$result = null;


$currentPageNo = 1; 

$pageLimit = 10;

$start = 0 ;
$searchVal = null;

$countQuery = "SELECT COUNT(*) as count FROM USERS";

$countResult = mysqli_query($db,$countQuery);

$totalUsers = mysqli_fetch_assoc($countResult)["count"];

echo "<script>console.log('".$totalUsers."')</script>";

$sql = "SELECT * FROM USERS order by create_date desc LIMIT $pageLimit OFFSET 0"; // if start is not set let it be used

if($_SERVER["REQUEST_METHOD"] == "POST"){

  if(isset($_POST["start"])){
    $start = $_POST["start"];
  }

  if(isset($_POST["page_limit"])){
    $pageLimit = intval($_POST["page_limit"]);
  }

  if(isset($_POST["currentPageNo"])){
    $currentPageNo = intval($_POST["currentPageNo"]);
  }

  echo "<script>console.log('current Page no = $currentPageNo')</script>";

  if(isset($_POST["search_val"])){
      $searchVal = mysqli_real_escape_string($db,$_POST["search_val"]);

      $sql = "SELECT * FROM USERS WHERE name like '$searchVal%' or user_id like '%$searchVal%' or phone like '$searchVal%' limit $pageLimit OFFSET $start";
  }
  else if(isset($_POST["start"])){
    $sql = "SELECT * FROM USERS order by create_date desc LIMIT $pageLimit OFFSET $start";
  }
  

  echo "<script>console.log('result = $result ')</script>";
}

$result = mysqli_query($db,$sql);


?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- This page plugin CSS -->
    <link href="assets/css/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include('ui/sidebar.php')
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
        <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title">App Users</h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                  </ol>
                </nav>
              </div>
            </div>
         
          </div>
        </div>
        <!-- -------------------------------------------------------------- -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <div class="container-fluid">
          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->
          <div class="widget-content searchable-container list">
            <!-- ---------------------
                        start Contact
                    ---------------- -->
            <div class="card card-body">
              <div class="row">
                <div class="col-lg-12">
                  <form action="" method="POST">
                    <div class="row">
                      <div class="col-md-6 col-xl-2">
                        <input
                          type="text"
                          name="search_val"
                          class="form-control"
                          placeholder="Search Users..."
                        />
                      </div>
                      <div class="col-md-4 col-xl-2">
                        <button class="btn btn-info">
                          <i data-feather="search" class="feather-sm fill-white me-1"> </i>
                            Search
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-----------------------
            end Contact
            ------------------>
            <!-- Modal -->  

            
            <?php
              echo "<p>$start-".($start + mysqli_num_rows($result))." out of $totalUsers</p>";
            ?>
            <div class="card card-body">
              <div class="table-responsive">
                <table class="table search-table v-middle text-nowrap">
                  <thead class="header-item">
                    <th>Phone</th>
                    <th>Name</th>
                    <th>Action</th>
                  </thead>
                  <tbody>
                    <?php

                      if($result==null){

                      }
                      else{
                    
                      while($row = mysqli_fetch_array($result)){
                        echo "<tr>";
                        echo '<td><span class="usr-ph-no">'.$row["user_id"].'</span></td>';
                        echo '<td>
                                <div class="d-flex align-items-center">
                                '.
                                ($row["profile_picture"]==null?
                                ""
                                :
                                "<img
                                  src='".$row["profile_picture"]."'
                                  alt='avatar'
                                  class='rounded-circle'
                                  width='40'
                                  height='40'
                                />").
                                '
                                <div class="ms-2">
                                  <div class="user-meta-info">
                                    <h5 class="user-name mb-0" data-name="Emma Adams">'.$row["name"].'</h5>
                                    <span class="user-work text-muted">'.$row["about"].'</span>
                                  </div>
                                </div>
                              </div>
                            </td>';
                        echo '<td>
                                <div class="action-btn">
                                  <form action="chat-list.php" method="POST">
                                    <input name="user_id" value="'.$row["user_id"].'" type="hidden" />
                                    <button style="border:none;backgroundColor:white;color:white" class="text-info edit">
                                      <i data-feather="eye" class="feather-sm fill-white"></i>
                                    </button>
                                  </form>
                                </div>
                              </td>';
                        echo "</tr>";
                        
                      }
                    }
                    ?>
        
               

                 
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <style>
            .dropdown {
              position: relative;
              display: inline-block;
            }

            .dropdown-content {
              display: none;
              position: relative;
              background-color: #f9f9f9;
              min-width: 160px;
              box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
              padding: 12px 16px;
              z-index: 1;
            }

            .dropdown:hover .dropdown-content {
              display: block;
            }
          </style>

          
          <div class="row">
            <div class="col-md-2 col-lg-2 col-xs-6 col-sm-6">
              <!-- Example split danger button -->

              <div class="dropdown">
                <button class='btn btn-primary'><?php echo $pageLimit; ?> Results per page</button>

                <div class="dropdown-content">
                <?php 

                  $dropDownValues = array(10,20,30,40,50,100);

                  foreach($dropDownValues as $dropDown){

                    echo "
                      <p>
                        <form id='page_limit_form_$dropDown' action='' method='POST'>
                          <input type='hidden' name='start' value='".(intval($start))."' />
                          <input type='hidden' name='currentPageNo' value='".($currentPageNo)."' />
                          <input type='hidden' name='page_limit' value='$dropDown' />
                          <a onclick='page_limit_form_$dropDown.submit()'>$dropDown</a>
                        </form>
                      </p>
                    ";

                  }

                ?>
                </div>
              </div>
            </div>
            <div class="col-md-10 col-lg-10 col-xs-6 col-sm-6">
              <nav aria-label="...">
                <ul class="pagination">
                  <li class="page-item <?php if($currentPageNo<3){echo "disabled";} ?>">
                    <?php
                      echo "
                            <form id='minus_two_form' action='' method='POST'>
                              <input type='hidden' name='start' value='".(intval($start)-($pageLimit*2))."' />
                              <input type='hidden' name='currentPageNo' value='".($currentPageNo-2)."' />
                              <input type='hidden' name='page_limit' value='".($pageLimit)."' />
                              <span class='page-link' onclick='minus_two_form.submit()'>Previous</span>
                            </form>
                          ";
                      
                    ?>
                  </li>
                  <?php 
                    if($currentPageNo!=1){
                      echo "<li class='page-item'>
                              <form id='minus_one_form' action='' method='POST'>
                                <input type='hidden' name='start' value='".(intval($start)-$pageLimit)."' />
                                <input type='hidden' name='currentPageNo' value='".($currentPageNo-1)."' />
                                <input type='hidden' name='page_limit' value='".($pageLimit)."' />
                                <span class='page-link' onclick='minus_one_form.submit()' href='javascript:$(plus_one_form).submit();'>".($currentPageNo- 1)."</span>
                              </form>
                            </li>";
                    }
                  ?>
                  <li class="page-item active">
                    <span class="page-link">
                      <?php echo $currentPageNo ?><span class="sr-only">(current)</span>
                    </span>
                  </li>
                  <li class="page-item">

                      <?php

                        if(mysqli_num_rows($result)==$pageLimit){

                            echo "<form id='plus_one_form' action='' method='POST'>
                                    <input type='hidden' name='start' value='".(intval($start)+$pageLimit)."' />
                                    <input type='hidden' name='currentPageNo' value='".($currentPageNo+1)."' />
                                    <input type='hidden' name='page_limit' value='".($pageLimit)."' />
                                    <span class='page-link' onclick='plus_one_form.submit()' >".($currentPageNo+1)."</span>
                                  </form>";
                        }
                      ?>
                  </li>
                  <li class="page-item <?php if(mysqli_num_rows($result)<$pageLimit){echo "disabled";} ?>" >
                        
                        <?php echo "<form id='plus_two_form' action='' method='POST'>
                                      <input type='hidden' name='start' value='".(intval($start)+($pageLimit*2))."' />
                                      <input type='hidden' name='currentPageNo' value='".($currentPageNo+2)."' />
                              <input type='hidden' name='page_limit' value='".($pageLimit)."' />
                                      <span class='page-link' onclick='plus_two_form.submit()' >Next</span>
                                    </form>";
                        ?>
                  </li>
                </ul>
              </nav>
            </div>
        </div>
            
          <!-- -------------------------------------------------------------- -->
          <!-- End PAge Content -->
          <!-- -------------------------------------------------------------- -->
        </div>
        
        <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
          <footer class="footer text-center">
            All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <!--<script src="assets/js/pages/contact/contact.js"></script> -->
  </body>
</html>