
<?php
  include("session.php");
  include("encryption/index.php");
  include("files/getFile.php");

  $username = "";
  $otherUserId = null;

  if($_SERVER["REQUEST_METHOD"]=="POST"){
    $username = mysqli_real_escape_string($db,$_POST["user_id"]);
    if(isset($_POST["other_user"])){
      $otherUserId =  mysqli_real_escape_string($db,$_POST["other_user"]);
    }
  }
  else if($_SERVER["REQUEST_METHOD"]=="GET"){
    $username = mysqli_real_escape_string($db,$_GET["user_id"]);
    if(isset($_GET["other_user"])){
      $otherUserId = mysqli_real_escape_string($db,$_GET["other_user"]);
    }
  }

  $otherUserProfile = array();

  $chatMessages = array();

  if($otherUserId!=null){

    $getOtherUserProfileSql = "SELECT * FROM USERS WHERE user_id='$username'";
    $getMessagesSql = "SELECT * FROM messages WHERE 
                      (receiver_database_id='$username' and sender_database_id='$otherUserId')
                      or
                      (receiver_database_id='$otherUserId' and sender_database_id='$username')
                      order by sent_timestamp;
                      ";

    $userMessagesResult = mysqli_query($db,$getMessagesSql);
    $otherUserProfileData = mysqli_query($db,$getOtherUserProfileSql);
    
    $otherUserProfile = mysqli_fetch_assoc($otherUserProfileData);

    while($row=mysqli_fetch_array($userMessagesResult)){
        echo "<script>console.log('".$row["message_type"]."') </script>";
        array_push($chatMessages,$row);
    }

  }
  else{
    header("location: app-contacts.php");
  }

  

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta
      name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta
      name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- This page css -->
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
      include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php 
        include("ui/sidebar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->

      <script>
        function scrollToBottom () {
          const id = "chat_comp";
          var div = document.getElementById(id);
          const scrollHeight = div.scrollHeight;
          console.log("Scroll Height = ",scrollHeight);
          div.scrollTop = div.scrollHeight;
        }
        
        
        //setTimeout(scrollToBottom, 1000);
              

        var messages = [];
        var sortedMessages = [];
        var userName = null;

        function setUserName(name){
          userName = name;
        }

      
        function clearMessages(){
          messages = [];
          sortedMessages = [];
        }

        function addNewMessage(messageType,sender_database_id,receiver_database_id,textMessage,mediaUrl,timestamp){
          messages.push({messageType,textMessage,sender_database_id,receiver_database_id,mediaUrl,timestamp});
          sortedMessages.push({messageType,textMessage,sender_database_id,receiver_database_id,mediaUrl,timestamp});
          addToUI();
        }

        function search(){
          var val = document.getElementById("searchInputVal").value;
          console.log("Searching val = ",val);
          sortedMessages = []; 
          for(var i=0;i<messages.length;i++){
            if((messages[i]["messageType"]===val) || (messages[i]["textMessage"] === val) || (messages[i]["messageType"]==="text" && messages[i]["textMessage"].toString().toLowerCase().includes(val.toLowerCase()))){
              sortedMessages.push(messages[i]);
            }   
          }
          addToUI();
        }

        function target_popup(form) {
          window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
          form.target = 'formpopup';
        }    

        function getMediaHtml(messageType,mediaUrl,type){
          //console.log("Url = ",mediaUrl);

          switch(messageType){
            case "image" : return `<form method='POST' action='chats/image_player.php' onsubmit='target_popup(this)'>
                                      <input type='hidden' value='${mediaUrl}' name='mediaUrl' >
                                      <button>
                                        <img height="100" width="100" src="assets/images/image_icon.png" /></a>
                                      </button>
                                  </form>`;
                                  break;
            case "audio" : return `<form method='POST' action='chats/audio_player.php' onsubmit='target_popup(this)'>
                                      <input type='hidden' name='mediaUrl' value='${mediaUrl}' />
                                      <button>
                                        <img height="100" width="100" src="assets/images/audio_icon.png" />
                                      </button>
                                    </form>`;
                                    break;
            case "video" : return `<form method='POST' action='chats/video_player.php' onsubmit='target_popup(this)'>
                                      <input type='hidden' name='mediaUrl' value='${mediaUrl}' />
                                      <button>
                                        <img height="100" width="100" src="assets/images/video_icon.png" />
                                      </button>
                                    </form>
                                  `;
                                  break;
            case "file"  : return `
                                    <form method='POST' action='chats/file_download.php' onsubmit='target_popup(this)' >
                                      <input type='hidden' name='mediaUrl' value='${mediaUrl}' />
                                      <button>
                                        <img height="100" 
                                             width="100" 
                                             src="assets/images/document_icon.png" 
                                             style='color:${type==='odd'?'white':'blue'}' />
                                      <br /><br /> 
                                      Click To Download
                                      </button>
                                    </form>`;
                                    break;
          }

          return "null";
        }

        function addToUI(){

          var html = "";

          console.log("sorted messages = ",sortedMessages);

          for(var i=0;i<sortedMessages.length;i++){
            var message = sortedMessages[i];

            console.log("Sender database id = ",message["sender_database_id"]," username = ",userName);

            if(message["sender_database_id"]!==userName){
              
              html += `<li class="chat-item">
                        <div class="chat-content">
                          <div class="box bg-light">
                            ${message["messageType"]=="text"?message["textMessage"]: getMediaHtml( message["messageType"],message["mediaUrl"],"even") }
                          </div>
                        </div>
                        <div class="chat-time">${new Date(message['timestamp']).toLocaleString()}</div>
                      </li>
                      `;
            }
            else{
              
              html += `<li class="odd chat-item">
                            <div class="chat-content">
                              <div class="box bg-light-inverse">${message["messageType"]=="text"?message["textMessage"]: getMediaHtml(message["messageType"],message["mediaUrl"],"odd") }</div>
                              <br />
                            </div>
                            <div class="chat-time">${new Date(message['timestamp']).toLocaleString()}</div>
                          </li>
                        `;
            }
                      
          }

        
          document.getElementById("messages-list").innerHTML = html;
          setTimeout(scrollToBottom, 1000);

        }

      </script>

      <?php

        echo "<script>setUserName('$username');</script>";

      ?>
      

      <div class="page-wrapper">
        <div class="chat-application">
        <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $username ?> - <?php echo $otherUserId ?></h4>
                  <div class="chat-box scrollable position-relative" style="height: 475px" id="chat_comp">

                  
                    <!--chat Row -->
                    <ul class="chat-list" id="messages-list">
                    <?php

                    echo "<script>clearMessages()</script>";

                    foreach($chatMessages as $message){

                      $messageType = $message["message_type"]; 

                      $textMessage = NULL;

                      $mediaUrl = NULL;

                      if($messageType =="image"){
                        $mediaUrl = $message["image_url"];
                      }
                      else if($messageType=="video"){
                        $mediaUrl = $message["video_url"];
                      }
                      else if($messageType=="audio"){
                        $mediaUrl = $message["audio_url"];
                      }
                      else if($messageType=="file"){
                        $mediaUrl = $message["file_url"];
                      }

                      $mediaUrl = $mediaUrl == null ? null : decryptMessage($mediaUrl);

                      if($mediaUrl==NULL){
                        if($messageType!="contact"){
                          $textMessage = decryptMessage($message["text_message"]);
                        }
                        else{
                          $textMessage = decryptMessage($message["contact_json"]);
                          $messageType = "text";
                        }
                      }

                      try{
                        //echo "<script>console.log('its working message = $textMessage')</script>";
                        echo "<script>addNewMessage('$messageType','".$message['sender_database_id']."','".$message['receiver_database_id']."','".$textMessage."', '".$mediaUrl."' ,".intval($message['sent_timestamp']).")</script>";
                      }
                      catch(Exception $e){
                        echo "<script>console.log('the error is ".$e->getMessage()."');";
                      }
                    }

                    echo "<script>addToUI();</script>";

                  ?>
                     
                    </ul>
                  </div>
                </div>
              </div>

              

            <div class="card card-body">
              <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                      <div class="col-md-3 col-xl-2">
                        <input
                          type="text"
                          id="searchInputVal"
                          name="search_val"
                          class="form-control"
                          placeholder="Search Messages..."
                        />
                      </div>
                      <div class="col-md-2 col-xl-2">
                        <button class="btn btn-info" onclick="search()">
                          <i data-feather="search" class="feather-sm fill-white me-1"> </i>
                            Search
                        </button>
                      </div>
                      <div class="col-md-2 col-xl-2">
                        <form action="calls.php" method="POST" >
                          <?php 
                            echo '<input type="hidden" name="user_id" value="'.$username.'" />';
                            echo '<input type="hidden" name="other_user" value="'.$otherUserId.'" />'; 
                          ?>
                          <button class="btn btn-info" type="submit">
                              Open Calls
                          </button>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          <!-- -------------------------------------------------------------- -->
          <!-- Left Part  -->
          <!-- -------------------------------------------------------------- -->
          
          <!-- -------------------------------------------------------------- -->
          <!-- End Left Part  -->
          <!-- -------------------------------------------------------------- -->
          <!-- -------------------------------------------------------------- -->
          <!-- Right Part  Mail Compose -->
          <!-- -------------------------------------------------------------- -->
        
        </div>

        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
          All Rights Reserved by Echo
        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
  
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/extra-libs/taskboard/js/jquery.ui.touch-punch-improved.js"></script>
    <script src="assets/extra-libs/taskboard/js/jquery-ui.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="assets/libs/popper.js/popper.min.js"></script> -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/pages/chat/chat.js"></script>
  </body>
</html>
