<?php

include('session.php');

if($_SESSION["user_role"]!="admin"){
  die("Access denied");
}

$username = null;
$password = null;
$user_role = null;
$message = null;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    if(isset($_POST["username"])){
        $username = $_POST["username"];
    }
    if(isset($_POST["password"])){
        $password = $_POST["password"];
    }

    if(isset($_POST["user_role"])){
      $user_role = $_POST["user_role"];
    }
    
}
else if($_SERVER["REQUEST_METHOD"]=="GET"){
    if(isset($_GET["username"])){
        $username = $_GET["username"];
    }
    if(isset($_GET["password"])){
        $password = $_GET["password"];
    }
    if(isset($_GET["user_role"])){
      $user_role = $_GET["user_role"];
    }
}

if($username != null && $password !=null ){
    $password = md5($password);
    $sql = "INSERT INTO admin_user VALUES ('$username','$password','$user_role')";
    $result = mysqli_query($db,$sql);
    if($result){
        $message = "User inserted succesfully";
        header("Location: admin_users.php");
    }
    else{
        $message = "Failed to add user";
    }
    echo "<script>console.log('$result')</script>";
}


?>


<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords"
      content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, xtreme admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, material design, material dashboard bootstrap 5 dashboard template"
    />
    <meta name="description"
      content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design"
    />
    <meta name="robots" content="noindex,nofollow" />
    <title>Echo Chat</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png" />
    <!-- This page plugin CSS -->
    <link href="assets/css/dataTables.bootstrap4.css" rel="stylesheet"/>
    <!-- Custom CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- -------------------------------------------------------------- -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- -------------------------------------------------------------- -->
    <?php
        include("ui/preloader.php");
    ?>
    <!-- -------------------------------------------------------------- -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- -------------------------------------------------------------- -->
    <div id="main-wrapper">
      <!-- -------------------------------------------------------------- -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include("ui/navbar.php")
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Topbar header -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <?php
        include('ui/sidebar.php')
      ?>
      <!-- -------------------------------------------------------------- -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- -------------------------------------------------------------- -->
      <!-- -------------------------------------------------------------- -->
      <!-- Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
      <div class="page-wrapper">
           <!-- -------------------------------------------------------------- -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- -------------------------------------------------------------- -->
        <div class="page-breadcrumb">
          <div class="row">
            <div class="col-5 align-self-center">
              <h4 class="page-title"> Add New Admin User</h4>
              <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Admin Users</li>
                  </ol>
                </nav>
              </div>
            </div>


        </div>

        <br />

        <div class="container-fluid">

        
            <?php 
                if($message!=null){
                    echo "<div class='alert alert-primary' role='alert'>";
                    echo $message;
                    echo "</div>";
                }
            ?>

          <!-- -------------------------------------------------------------- -->
          <!-- Start Page Content -->
          <!-- -------------------------------------------------------------- -->
          <div class="widget-content searchable-container list">
           <form action="" method="POST">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="">Username</span>
                    </div>
                    <input type="email" name="username" class="form-control">
                </div>  
                <br />
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="">Password</span>
                    </div>
                    <input type="text" name="password" class="form-control">
                </div>
                <br />
                <!-- Example single danger button -->
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="">User Role</span>
                    </div>
                    <select name="user_role" class="form-control">
                        <option>viewer</option>
                        <option>admin</option>
                    </select>
                </div>
                <br />
                <br />
                <br />
                <button type="submit" class="btn btn-primary">Add User</button>
            </form>
            <br />
            <form action="admin_users.php">
            <button type="submit" class="btn btn-danger">Close</button>
            </form>
        </div>
    

          <!-- -------------------------------------------------------------- -->
        <!-- End Container fluid  -->
        <!-- -------------------------------------------------------------- -->
        <!-- -------------------------------------------------------------- -->
        <!-- footer -->
        <!-- -------------------------------------------------------------- -->
        <footer class="footer text-center">
          <footer class="footer text-center">
            All Rights Reserved by Echo
        </footer>        </footer>
        <!-- -------------------------------------------------------------- -->
        <!-- End footer -->
        <!-- -------------------------------------------------------------- -->
      </div>
      <!-- -------------------------------------------------------------- -->
      <!-- End Page wrapper  -->
      <!-- -------------------------------------------------------------- -->
    </div>
    <!-- -------------------------------------------------------------- -->
    <!-- End Wrapper -->
    <!-- -------------------------------------------------------------- -->
    <!-- -------------------------------------------------------------- -->
    <!-- customizer Panel -->
    <!-- -------------------------------------------------------------- -->
    
    <div class="chat-windows"></div>
    <!-- -------------------------------------------------------------- -->
    <!-- Required Js files -->
    <!-- -------------------------------------------------------------- -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- Theme Required Js -->
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/app.init.js"></script>
    <script src="assets/js/app-style-switcher.js"></script>
    <!-- perfect scrollbar JavaScript -->
    <script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/js/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="assets/js/feather.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
    <!-- --------------------------------------------------------------- -->
    <!-- This page JavaScript -->
    <!-- --------------------------------------------------------------- -->
    <!--<script src="assets/js/pages/contact/contact.js"></script> -->
  </body>
</html>