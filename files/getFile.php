<?php 


function getNetworkFileAsBase64($url){

  $file_contents = file_get_contents($url);
  
  $encoded = base64_encode($file_contents);

  return $encoded;

}


/*
$url = 'https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643143589826232..jpg1643143591170.jpg?alt=media&token=0daf12f8-ef31-4011-b547-deec16f0df42';
      
// Use basename() function to return the base name of file
$file_name = "a-download.jpg";

echo "<script>console.log('$file_name')</script>";
      
// Use file_get_contents() function to get the file
// from url and use file_put_contents() function to
// save the file by using base name
$file_contents = file_get_contents($url);

$base64Data = base64_encode($file_contents);

echo base64_encode($file_contents);

if (file_put_contents($file_name, decryptMessage($byteArray))){
    echo "<script>console.log('File downloaded successfully')</script>";
}
else{
    echo "<script>console.log('File downloading failed.');</script>";
}
/*

const url = "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643143589826232..jpg1643143591170.jpg?alt=media&token=0daf12f8-ef31-4011-b547-deec16f0df42";

fetch(url,{
  mode: 'cors',
  headers: {
    'Access-Control-Allow-Origin':'*'
  }
})
  .then(response => response.text())
  .then(data => {
  	// Do something with your data
  	console.log(data);
  });

*/

?>