<?php

include("../session.php");
include("../encryption/index.php");
include("../files/getFile.php");
include("getDecryptedFileData.php");

$mediaUrl = null;
$fileData = null;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $mediaUrl =  $_POST["mediaUrl"];

    if($mediaUrl!=NULL){
        $fileData = getdecryptedData($mediaUrl);
    }
}


?>

<html>
    <head>
        <script type='text/javascript' src='../assets/js/download.js'></script>
        <script type="module">

            import mime from "https://cdn.skypack.dev/mime";

            var mediaUrl = null;

            <?php
                
                if($mediaUrl==null){
                    die("Media url not found");
                }

                if($fileData==null){
                    die("Unable to fetch data");
                }

                echo "mediaUrl='$mediaUrl';";

            ?>

            if(mediaUrl==null){
                window.alert("Media url not found");
                exit(0);
            }

            var fileExtension = new URL(mediaUrl).pathname.split(".").pop();

            var mimeType = mime.getType(fileExtension);

            console.log("Mime type = ",mimeType);

            var downloadUrl = `data:${mimeType};base64,<?php echo $fileData ?>`;

            download(downloadUrl,`${Date.now()/1000}.${fileExtension}`,mimeType);
            
        </script>
    </head>  
    <body>
        Your download will begin shortly
    </body>
</html>