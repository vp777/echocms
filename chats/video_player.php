<?php


include("../session.php");
include("../encryption/index.php");
include("../files/getFile.php");
include("getDecryptedFileData.php");


$mediaUrl = NULL;
$fileData = NULL;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $mediaUrl =  $_POST["mediaUrl"];

    echo "<script>console.log('$mediaUrl');</script>";

    if($mediaUrl!=NULL){
        $fileData = getdecryptedData($mediaUrl);
    }
}

if($fileData == null){
    echo "File Data not found";
}
else{
    echo "<html>
            <body style='background-color:black'>
                <center>
                    <video width='320' height='240' controls>
                        <source src='data:video/mp4;base64,$fileData' type='video/mp4'>
                            Your browser does not support the video tag.
                    </video>
                </center>
            </body>
        </html>";
}

?>