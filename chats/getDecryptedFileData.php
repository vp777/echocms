<?php


function getdecryptedData($fileUrl){
    $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/decryptFile";

    //The data you want to send via POST
    $fields = [
      'url' => $fileUrl // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
    ];

    //url-ify the data for the POST 
    $fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data   
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

    //execute post
    $result = curl_exec($ch);

    echo "<script>console.log('$result')</script>";

    return $result;
  }

?>