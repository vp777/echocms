<?php


include("../session.php");
include("../encryption/index.php");
include("../files/getFile.php");
include("getDecryptedFileData.php");


$mediaUrl = NULL;
$fileData = NULL;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $mediaUrl =  $_POST["mediaUrl"];

    echo "<script>console.log('$mediaUrl');</script>";

    if($mediaUrl!=NULL){
        $fileData = getdecryptedData($mediaUrl);
    }
}

if($fileData == null){
    echo "File Data not found";
}
else{
    echo "<html>
            <body style='background-color:black'>
                <center>
                    <audio autostart='0' autostart='false' preload='none' controls='controls' autobuffer='autobuffer' >
                        <source src='data:audio/wav;base64,$fileData' />
                    </audio>
                </center>
            </body>
        </html>";
}

?>