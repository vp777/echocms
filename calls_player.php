<?php 

include("session.php");
include("encryption/index.php");

function getBase64Data($fileKey){
  $url = "https://us-central1-echochatapp-6d1ca.cloudfunctions.net/getAudioFileData";

  //The data you want to send via POST
  $fields = [
    'fileKey' => $fileKey // "https://firebasestorage.googleapis.com/v0/b/echochatapp-6d1ca.appspot.com/o/images%2F1643230169813602..jpg1643230169878.jpg?alt=media&token=cff08cbf-a81e-4fbc-b7ed-9b7c1ff93326"
  ];

  //url-ify the data for the POST 
  $fields_string = http_build_query($fields);

  //open connection
  $ch = curl_init();

  //set the url, number of POST vars, POST data   
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_POST, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

  //So that curl_exec returns the contents of the cURL; rather than echoing it
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

  //execute post
  $result = curl_exec($ch);

  echo "<script>console.log('.$result.')</script>";

  return $result;
}

?>

<?php

    $fileName = null;
    $errMessage = null;
    $fileBase64Data = null;

    if($_SERVER["REQUEST_METHOD"]=="GET"){
        $fileName = mysqli_real_escape_string($db,$_GET["fileName"]);
        if($fileName!=null){
            $fileBase64Data = getBase64Data($fileName);
        }
    }


    if($fileName==null){
        echo "Invalid file name";
    }
    else if($fileBase64Data==null){
        echo "File could be converted to required  format";
    }
    else{
        echo "<html>
                <style>
                    .spinner-wrapper {
                        position: fixed;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        background-color: #ff6347;
                        z-index: 999999;
                    }
                    .spinner{
                        position: absolute;
                        top: 48%;
                        left: 48%;
                    }
                </style>
                <script src='assets/js/jquery.min.js'></script>
                <script>
                    $(document).ready(function() {
                        //Preloader
                        preloaderFadeOutTime = 500;
                        function hidePreloader() {
                            var preloader = $('.spinner-wrapper');
                            preloader.fadeOut(preloaderFadeOutTime);
                        }
                        hidePreloader();
                    });
                </script>
                <body style='background-color:black'>
                    <div class='spinner-wrapper'>
                        <div class='spinner'></div>
                    </div>
                    <center>
                        <video controls>    
                            <source src='data:video/mp4;base64,$fileBase64Data' type='video/mp4'>
                                Your browser does not support the video tag.
                        </video>    
                    </center>
                </body>
             </html>";
    }
?>