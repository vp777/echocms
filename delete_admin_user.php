<?php

include('session.php');


if($_SESSION["user_role"]!="admin"){
    die("Access denied");
}

$username = null;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    if(isset($_POST["username"])){
        $username = $_POST["username"];
    }
}
else if($_SERVER["REQUEST_METHOD"]=="GET"){
    if(isset($_GET["username"])){
        $username = $_GET["username"];
    }
}


if($username==null){
    die("Username not found");
}

$sql = "DELETE FROM admin_user where user_id='$username'";

$result = mysqli_query($db,$sql);

if(!$result){
    die("Failed to delete");
}


?>

<html>
    <head>
    </head>
    <body>
        <script>    
            window.alert("<?php echo "Admin User with user id $username has been deleted succesfully" ?>");
            window.location.href = "admin_users.php";
        </script>
    </body>
</html>